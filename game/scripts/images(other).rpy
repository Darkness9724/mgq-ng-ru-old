# одноцветные изображения
image bg black = Solid("#000")
image bg white = Solid("#fff")
image color black = Solid("#000")
image color white = Solid("#fff")

# изображения окрашенные в один тон
image bg 064 grayscale = im.Grayscale("images/Backgrounds/bg 064.webp")
image bg 151 grayscale = im.Grayscale("images/Backgrounds/bg 151.webp")
image bg 155 grayscale = im.Grayscale("images/Backgrounds/bg 155.webp")
image bg 002 grayscale = im.Grayscale("images/Backgrounds/bg 002.webp")
image bg 007 grayscale = im.Grayscale("images/Backgrounds/bg 007.webp")

image lazarus st01 grayscale = im.Grayscale("images/Characters/lazarus/lazarus st01.webp") #700
image mob ozisan1 grayscale = im.Grayscale("images/Characters/mob/mob ozisan1.webp") #700
image mob sinkan grayscale = im.Grayscale("images/Characters/mob/mob sinkan.webp") #700

# изображение нотки
image note:
    im.Composite((20, 19),
        (0, 0), im.MatrixColor("images/System/note.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (2, 2), im.MatrixColor("images/System/note.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (0, 2), im.MatrixColor("images/System/note.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (2, 0), im.MatrixColor("images/System/note.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (1, 1), im.MatrixColor("images/System/note.webp",
                                im.matrix.colorize(persistent.what_color, persistent.what_color)))

image ctc_wait:
    LiveComposite((24, 23),
        (8, 6), im.MatrixColor("images/System/ctc_wait.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (7, 4), im.MatrixColor("images/System/ctc_wait.webp",
                                im.matrix.colorize(persistent.what_color, persistent.what_color)))
    0.2

    LiveComposite((24, 23),
        (8, 8), im.MatrixColor("images/System/ctc_wait.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (7, 6), im.MatrixColor("images/System/ctc_wait.webp",
                                im.matrix.colorize(persistent.what_color, persistent.what_color)))
    0.2
    repeat

image ctc:
    LiveComposite((24, 23),
        (8, 6), im.MatrixColor("images/System/ctc.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (7, 4), im.MatrixColor("images/System/ctc.webp",
                                im.matrix.colorize(persistent.what_color, persistent.what_color)))
    0.2

    LiveComposite((24, 23),
        (8, 8), im.MatrixColor("images/System/ctc.webp",
                                im.matrix.colorize(persistent.what_out_color, persistent.what_out_color)),
        (7, 6), im.MatrixColor("images/System/ctc.webp",
                                im.matrix.colorize(persistent.what_color, persistent.what_color)))
    0.2
    repeat

image face = TransitionConditionSwitch(Dissolve((.5), alpha=True),
    "mdg_paralize == 1",
        "images/System/face_02.webp",
    "player == 0 and kousoku == 0",
        "images/System/face_0[status].webp",
    "player == 0 and kousoku > 0",
        "images/System/face_1[status].webp",
    "player == 1",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_a00.webp"),
    "player == 2",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_b00.webp"),
    "player == 3",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_c00.webp"),
    "player == 4",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_d00.webp"),
    "player == 5",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_e00.webp"),
    "player == 99 and kousoku > 0",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_x00.webp",
            (0, 0), "images/System/face_x10.webp"),
    "player == 99 and status > 0",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_x00.webp",
            (0, 0), "images/System/face_x0[status].webp"),
    "player == 99",
        LiveComposite((66, 60),
            (3, 5), "images/System/face_x00.webp"))