label hansei:
    hide screen hp
    play music "audio/bgm/irias.ogg"
    scene bg 001 with Dissolve(3.0)
    $ renpy.call(lavel + "_hansei")
    stop music fadeout 1
    scene bg black with Dissolve(3.0)
    return


label slime_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Oh come on, you're already here?{w}\nLosing to the first enemy... how pathetic!"

    show ilias st01 with dissolve #700

    i "Beaten by a Slime... the punching bag enemy of RPGs!"
    i "Remember to use {i}Struggle{/i} if you are bound.{w}\nAs long as you are able to move, it's an easy fight."
    i "If you are bound when your HP is below half, the Slime has a small chance of using {i}Slime Heaven{/i}."
    i "But if you fight normally, this situation shouldn't occur..."

    show ilias st02 with dissolve #700

    i "Well then, Luka, it's time to try again.{w}\nI will always watch over you."

    return


label alice1_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Defeated effortlessly, huh?"

    show ilias st01 with dissolve #700

    i "As you are now, you have no chance of victory.{w}\nIf you can't find a way to avoid fighting her, you will never be able to move on."
    i "Never forget the disgrace you suffered here.{w}\nSooner or later, you must defeat this person."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nI shall wait for the day you destroy the Monster Lord..."

    return


label slug_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Beaten by a slug..."

    show ilias st01 with dissolve #700

    i "As long as you keep struggling, you should be ok."
    i "Even if you can't get her off of you too quickly, she isn't that powerful.{w}\nAs long as you don't hesitate, and continue attacking, you should be fine."

    show ilias st02 with dissolve #700

    i "Now go, oh Brave Luka.{w}\nDon't lose again to such a weak monster..."

    return


label mdg_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "So you became a Mandragora's food source...{w}\nLosing the fight and being treated such a way... how pathetic."

    show ilias st01 with dissolve #700

    i "In this fight, never doubt yourself, and never give up. Just continue to attack single-mindedly."
    i "If you do so, you will open your route to victory."
    i "Even more, you can avoid the battle completely by a previous choice."
    i "Completely avoiding a fight isn't something a Hero would do, though..."
    i "In addition, on {i}Normal{/i}, this enemy won't use a certain technique."
    i "With {i}Normal{/i}, it's only a scripted battle.{w}\nOn {i}Hard{/i}, it will also become a normal battle."
    i "Make sure you check the Monsterpedia to ensure you've learned everything about your opponent..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nWield your sword of justice in my name."

    return


label granberia1_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You were beaten by Granberia, one of the Four Heavenly Knights...?"

    show ilias st01 with dissolve #700

    i "Unfortunately, there is no way you can beat her.{w}\nHowever, if you use your strongest technique, you might open a road."

    show ilias st04 with dissolve #700

    i "...I can't easily forgive a Hero who uses the skill of a Monster, though.{w}\nOnly for you, Luka, will I overlook it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSomeday you will be able to defeat those four..."

    return


label mimizu_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You lost to an earthworm...{w}\nHow disgraceful."

    show ilias st01 with dissolve #700

    i "If you don't struggle, you will surely lose."
    i "If she coils around you when you are below half health, there's a 50 chance she will use {i}Sweet Worm{/i}."
    i "If you get caught in that attack, there is no escape."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDo not be beaten by the earthworm again..."

    return


label gob_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Beaten by such a tiny Goblin... how miserable!"

    show ilias st01 with dissolve #700

    i "You must remember to guard when you see your enemy use their strongest attack.{w}\nIf you do so, you will evade the attack."
    i "Make sure you keep the lessons you learned in this defeat in mind for future battles."
    i "In addition, the Goblin Girl uses different attacks on {i}Hard{/i}.{w}\nIf you want to fill out the Monsterpedia, take heed."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSmash down even monster children with your hammer of justice."

    return


label pramia_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by a Tiny Lamia like that..."

    show ilias st01 with dissolve #700

    i "As long as you struggle when restrained, you shouldn't lose.{w}\nSince you did not struggle, did you simply surrender to a monster...?"

    show ilias st04 with dissolve #700

    i "If you do not try, you cannot win."

    show ilias st01 with dissolve #700

    i "In addition, on {i}Hard{/i} there will be a normal fight.{w}\nPay attention to her moves if you face her in this way."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou mustn't allow monster bandits to go unpunished."

    return


label vgirl_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You became a vampire's prey?{w}\nHow embarrassing for a Hero..."

    show ilias st01 with dissolve #700

    i "When bound, Struggle.{w}\nWhen you sense your enemy using a powerful attack, Guard."
    i "As long as you remember this, you will surely win."
    i "There are two ways that this enemy can defeat you...{w}\nOne by her clinging to you, and the other through forced obedience..."
    i "You'll probably see both...{w} Oh what am I saying...?{w}\nI believe in you."
    i "In addition, the Vampire Girl has attacks she will only use on {i}Hard{/i}."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEvil beings like vampires... Slay them with your sword."

    return


label dragonp_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by a dragon like that... how pathetic."

    show ilias st01 with dissolve #700

    i "If a normal attack doesn't connect, why not try your strongest?"
    i "Surely, it's a horrible monster's technique...{w}\nBut it's still very powerful."
    i "It may just be the fate of the {i}Non-Blessed Hero{/i}..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nFor the people of Iliasburg, you must defeat the bandits!"

    return


label mitubati_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Indulging in the pleasures of a Bee Girl licking you...{w}\nWhat a shameless Hero."

    show ilias st01 with dissolve #700

    i "As long as your level is 6 or higher, it should be an easy fight."
    i "Just struggle when bound, and you should win with no effort."
    i "By the way, there are two ways to lose to the Bee Girl.{w}\nDepending on what move she beats you with, the scene may change..."
    i "...But you wouldn't be thinking such lewd things, would you?"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThe people of Happiness Village are waiting for your help..."

    return


label hapy_a_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did you enjoy mating with the Harpy?{w}\nHaving a child with a monster... What a pathetic Hero."

    show ilias st01 with dissolve #700

    i "When the Harpy flies in the air, you won't be able to hit her."
    i "But don't give up.{w}\nIf you wait, you will surely find your chance."
    i "You only have a moment, don't miss it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the monsters with your sword of justice."

    return


label hapy_bc_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "So you were made into their prey...{w}\nDo you really want to be a slave to monsters so badly?"

    show ilias st01 with dissolve #700

    i "This fight is similar to the previous Harpy.{w}\nYou need to wait for your chance to hit them."
    i "There's no way you couldn't have figured that out by yourself...{w}\nDid you lose on purpose to play with the sisters...?"

    show ilias st04 with dissolve #700

    i "...What a shameless Hero..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't lose to pests like these harpies again."

    return


label queenharpy_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Was mating with the Queen Harpy amazing?{w}\nDrowning in lust and tossing away your mission... how miserable."

    show ilias st01 with dissolve #700

    i "The Queen Harpy is a very powerful enemy.{w}\nYou will need to collect SP and use your best moves if you want to win."
    i "Like with the other harpies, you need to time your attacks carefully if you want to hit."
    i "Moreover, if you attack at a bad time, she will counterattack.{w}\nMake sure you pay attention to avoid this."
    i "It's a long fight, but if you pay attention you should be able to win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nShow the power of your justice to the leader of the harpies."

    return


label delf_a_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "A Hero becoming corrupted...{w}\nCan you see how sad this makes me?"

    show ilias st01 with dissolve #700

    i "The only troublesome ability of the Dark Elf is her {i}Kiss of Ecstasy{/i}.{w}\nIf you take this attack, you will be unable to attack for a few turns."
    i "Unfortunately, there is no way to prevent this...{w}\nThe best way to win is to go all out from the start."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDefeat those who have fallen to the dark, and send them to where they belong..."

    return


label delh_b_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Were you satisfied having all your energy taken by that tentacle?"

    show ilias st01 with dissolve #700

    i "If you are restrained, the monster shall absorb your energy.{w}\nNot only will you be injured, but she will recover."
    i "You must escape immediately.{w}\nLike the last fight, you must end this quickly..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nI am always watching over you..."

    return


label hiru_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Being sucked dry by a Leech Girl...{w}\nWhat a pitiful end..."

    show ilias st01 with dissolve #700

    i "If you are bound by this enemy, she will absorb your energy.{w}\nAs much as you are damaged, she will recover."
    i "If you don't finish the fight quickly, you will surely be overwhelmed."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nA true Hero would not be beaten by such a monster."

    return


label rahure_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Inseminating a plant and breaking the taboo...{w}\nThat's a serious crime, you know?"

    show ilias st01 with dissolve #700

    i "The Rafflesia Girl will intoxicate you with her aroma and drag you in."
    i "Try to avoid taking damage, and finish the fight as quickly as possible."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake such a disgusting plant monster wither."

    return


label ropa_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Preyed on by a horrible monster like that... Were you satisfied?{w}\nSoaking up the pleasure as she eats your body... How sad."

    show ilias st01 with dissolve #700

    i "Though the Roper doesn't have a special attack, her restraint is very powerful."
    i "Just fight as you do normally, and you should win."
    i "The more skills you learn, the more varied your strategies will be."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDo not allow yourself to be eaten by the Roper again..."

    return


label youko_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by such a weak little fox...{w}\nHave you already lost the will of a Hero...?"

    show ilias st01 with dissolve #700

    i "The Kitsune doesn't use many strong attacks."
    i "If you don't mess up too many times when she clones herself, you won't take too much damage."
    i "As long as you closely watch what's happening, you should be fine."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNever let such a tiny fox beat you again..."

    return


label meda_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Falling prey to a cave monster...{w}\nWere you happy at being squeezed to death?"

    show ilias st01 with dissolve #700

    i "If the Meda grabs you with her tentacle and brings you into her mouth, there is no escape.{w}\nBefore that happens, you must struggle and break free."
    i "As long as you avoid her mouth and continue to attack, you should win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAs a Hero, you are never to bend your knee to a monster..."

    return


label kumo_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did you become the Spider Girl's prey?{w}\nFeeling good while your whole body is wrapped in her silk... pathetic."

    show ilias st01 with dissolve #700

    i "During the battle, the Spider Girl will start to wrap your body in her web.{w}\nThere is no way to break her web, so it's useless to struggle."
    i "If you are wound four times, you will be unable to move, and fated to be her prey."
    i "Before that happens, you must keep attacking, and defeat her!"
    i "Moreover, the Spider Girl has two ending scenes depending on how she defeats you..."
    i "Are you going to lose on purpose to see the other scene?{w}\nIf so, I'll get angry..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDefeat that Spider Girl in one go..."

    return


label mimic_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You became the Mimic's prey? Pathetic...{w}\nDid you enjoy being eaten by such a hateful monster?"

    show ilias st01 with dissolve #700

    i "The Mimic has a very specific attack pattern."
    i "The most important point is to hold your attacks while the chest is closed. If you attack when she's in that state, she's sure to counter."
    i "If you hold your attack, you will surely find an opening sooner or later."
    i "Additionally, she will try to draw you into her chest... You must struggle with all of your might at that point."
    i "The Mimic has two defeat scenes, as well...{w}\nBut if you tried to see both of them on purpose, you wouldn't be a Hero, would you?"
    i "Anyway, the Mimic is a formidable enemy.{w}\nYou can always avoid the fight by not opening the chest in the first place."
    i "If you're unable to win no matter what, that's also an option...{w}{nw}"

    show ilias st04 #700
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nBut whether it's an acceptable choice for a Hero is another story..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCrush that insidious monster who tricks travelers..."

    return


label nanabi_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Played with by the fox monster? How pathetic..."

    show ilias st01 with dissolve #700

    i "...But she is an enemy too formidable for you to fight at the moment.{w}\nThere are several things you must keep in mind."
    i "She has too much HP for you to deal with as you are now, but there's no need to deal with all of it."
    i "Once you reduce it to a certain amount, something might happen..."
    i "In addition, she deals a lot of damage, so you must take care of your SP so you can use it when it's important."
    i "This is a long fight, so using your SP for healing yourself may be a wise move..."
    i "Lastly, the most annoying move, {i}Seven Moons{/i}."
    i "If you don't have enough HP and {i}Guard{/i}, you will surely be defeated by it."
    i "However, it takes several turns for Nanabi to accumulate enough power to use this ability, so you can heal yourself and prepare."
    i "After she uses the ability, she won't take any action for a few turns, so you have a chance to rebuild your defenses."
    i "To achieve victory, you will have to be careful with your timing."
    i "Though a trivial, useless point, there are two defeat scenes.{w}\nNot something a Hero would pursue, but do with the information what you wish."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEven against an enemy more powerful than you, never yield."

    return


label tamamo1_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "It looks like the queen of the Kitsunes defeated you..."

    show ilias st01 with dissolve #700

    i "As you are now, you cannot defeat this opponent.{w}\nYou need to find a way to avoid this fight."
    i "One day you will be able to face her on equal footing..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOne day you will be able to fight her..."

    return


label alma_elma1_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Abused by the succubus queen...{w}\nHow pitiful."

    show ilias st01 with dissolve #700

    i "She's too powerful for you as you are now.{w}\nHowever, if she's only using her tail, you may have a chance."
    i "You must be vigilant in your defense.{w}\nAttacking is good, but be careful not to be left without a way to heal yourself."
    i "She has a lot of HP, but you don't need to take it all down.{w}\nOnce you injure her, a path may open itself."
    i "Also if the battle extends too long, she may use an unusual technique..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nGive your judgement to such an indecent monster."

    return


label namako_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st01 with dissolve #700

    i "Defeated by a Sea Cucumber Girl...{w}\nHow pitiful."
    i "She follows a predictable pattern.{w}\nAs long as you struggle when bound, you should be fine."
    i "She has a dangerous attack that puts you into a trance, but she doesn't use it too often."
    i "Stick to the basics of combat, and you will come out on top."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't let the Sea Cucumber Girl squeeze you like that again..."

    return


label kai_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Forced into marriage...{w}\nAre you a Hero or a wimp!?"

    show ilias st01 with dissolve #700

    i "The Shellfish Girl has a strong binding attack.{w}\nShe will attack two turns for every one turn you get while bound."
    i "As long as you struggle when bound, you should come out on top."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nGet rid of that annoying monster..."

    return


label lamia_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did you become the prey of a Lamia?{w}\nHow pathetic..."

    show ilias st01 with dissolve #700

    i "The Lamia is strong, but as you are now, you should be able to win."
    i "If she constricts around you too tightly, you will never be able to escape."
    i "You must struggle as soon as you can, or you will surely lose."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBring your justice to that hideous snake."

    return


label lamiab_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did you become the prey of a Lamia?{w}\nMating endlessly... Are you happy at your end?"

    show ilias st01 with dissolve #700

    i "There really is no point to the Lamia taking off her clothes.{w}\nBut she's still a troublesome enemy."
    i "If you are constricted four times, you will lose.{w}\nStruggling is useless, so you must go all out in your attack!"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBring your justice to that hideous snake."

    return


label granberia2_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You were defeated by Granberia?{w}\nShe wasn't even trying... How pathetic."

    show ilias st01 with dissolve #700

    i "As you are now, you cannot defeat Granberia.{w}\nBut you should be able to drive her off."
    i "All she wants to do is see your abilities.{w}\nShow everything you've learned to her."
    i "If the fight lasts too long, she'll use a unique ability.{w}\nIf you want to complete the encyclopedia, make sure to note that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEventually you will be able to defeat Granberia..."

    return


label page17_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Losing to a monster like that...{w}\nWere you trying to fight normally, or something?"

    show ilias st01 with dissolve #700

    i "You can't fight Page 17 in a normal way.{w}\nThe only way to damage a slippery monster like that is with your fastest attack."
    i "In addition, she has a troublesome paralysis attack...{w}\nShe has very low HP, so you must finish the fight quickly!"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThough you don't need to read such a book...{w}\nStill, you must not let a monster get the best of you."

    return


label page257_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Surrendering to a monster's hands...{w}\nThen coming to me with such a composed face?"

    show ilias st01 with dissolve #700

    i "Page 257's huge arms are troublesome...{w}\nBut she doesn't use any other attack, so it's a very easy fight."
    i "If you use {i}Struggle{/i} properly, you can't be defeated."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEducate that horrible monster with your justice."

    return


label page65537_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did it feel good to be squeezed to death by her?{w}\nWas that really your desire?"

    show ilias st01 with dissolve #700

    i "Page 65537 is a formidable enemy with a lot of attacks."
    i "You'll want to save your SP for recovery, and slowly whittle down her health."
    i "Once you lower her HP, she may use a move that causes paralysis.{w}\nIt takes a few turns to prepare, so recover your HP in the meantime."
    i "If her HP gets even lower, she may use a move that forces you to submit to her."
    i "It deals a lot of damage, so take the time to prepare."
    i "You cannot guard through either of those skills, so don't waste a valuable turn in futility.."
    i "If you take care of your own HP and fight normally, you should be fine."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that monster who lives in a book."

    return


label kani_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Did it feel good being washed by the Crab Girl?{w}\nStuck forever as a slave to her technique...{w}\nDo I need to look for a different Hero?"

    show ilias st01 with dissolve #700

    i "But... If you still have the motivation to pick up your sword again..."
    i "When bound by the Crab Girl, she deals a lot of damage.{w}\nYou need to immediately start to struggle if you hope to win."
    i "However, you need to be careful when she offers you her special course.{w}\nWhen she begins her preparations, you need to attack her."
    i "Otherwise, you will surely be defeated."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nWhile I look for a replacement Hero..."

    return


label kurage_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Toyed with by the Jellyfish Girl until your death...{w}\nWere the thousands of tentacles in her umbrella pleasant?"

    show ilias st01 with dissolve #700

    i "The Jellyfish Girl can bind and paralyze you.{w}\nThere's no way to evade them, so you need to win as quickly as possible."
    i "Also, if you're being dragged into her umbrella, you need to attack instead of struggle."
    i "As long as you remember that, the fight should be easy."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nWhile I look for a replacement Hero..."

    return


label iso_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Did it feel good to be swallowed alive by the Sea Anemone Girl?"

    show ilias st01 with dissolve #700

    i "This battle is special in that you start bound.{w}\nIn addition, you cannot escape no matter how much you struggle."
    i "Saying that, you have to fight while restrained.{w}\nDon't waste any turns struggling when you don't need to."
    i "However, if she tries to drag you into her mouth, at that point you do need to struggle, otherwise she will drag you into her."
    i "If you follow those instructions, you should win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAt least, don't let another Hero take your place..."

    return


label ankou_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You were absorbed by the Anglerfish Girl?{w}\nWas it alright as long as you felt good in the end?"

    show ilias st01 with dissolve #700

    i "Like the last fight, this fight is special."
    i "First of all, a normal attack doesn't work.{w}\nIf you try to attack, you will see another method, though."
    i "In addition, special attacks don't work either."
    i "If she starts to absorb you, you must struggle.{w}\nBefore too much of your body sinks into her, you must escape."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIt's difficult finding a Hero to replace you..."

    return


label kraken_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Played with in your dirty hole by the Kraken... What a pathetic Hero."

    show ilias st01 with dissolve #700

    i "The Kraken is a particularly powerful enemy.{w}\nShe's able to attack two times every turn, and deal considerable damage."
    i "Be careful with your health, and use SP to recover."
    i "Most importantly, when you're restrained, you must struggle.{w}\nIf you're caught too long, you will take massive damage from her attacks."
    i "Lastly, after she uses {i}Targeting{/i}, you must {i}Guard{/i}."
    i "If you don't {i}Guard{/i}, you will take crippling damage.{w}\nCont to steadily damage her while being defensive."
    i "Though her HP is huge, you only need to decrease it a certain amount."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThere is no one who can take your place in this world..."

    return


label ghost_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by a ghost?{w}\nSuch a pathetic Hero..."

    show ilias st01 with dissolve #700

    i "Though the Ghost has strong attacks, she doesn't have any special moves."
    i "If you fight as you usually do, you can steadily win."
    i "But if you are restrained, you must struggle immediately.{w}\nIf you don't, you will lose any chance at victory in a single turn."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSend that Ghost Girl back to the afterlife..."

    return


label doll_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Did it really feel good to be raped by a doll?{w}\nHow shameful."

    show ilias st01 with dissolve #700

    i "If you're restrained by her hair, she can use an ability that drains your HP."
    i "So if you are bound, you need to struggle right away.{w}\nHer moves are damaging, so don't neglect your own HP."
    i "By the way, there are two different ways this doll can beat you...{w}\nBut as a Hero, you don't really need to worry about that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must destroy strange dolls like that..."

    return


label zonbe_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Delighted by being raped by a Zombie Girl? How disgusting..."

    show ilias st01 with dissolve #700

    i "Even though the Zombie Girl has a lot of HP, she isn't dangerous.{w}\nAs long as you struggle immediately, you won't be defeated."
    i "If you're playing on Normal, the only way to lose is on purpose."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn that moving corpse into an actual corpse..."

    return


label zonbes_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "A zombie orgy...{w}\nAre you really happy at that outcome?"

    show ilias st01 with dissolve #700

    i "Even if there are three of them, they're still just zombies.{w}\nUse your SP wisely for attack and defense, and defeat them one after another."
    i "If only one zombie remains, you must get her off of you as quickly as possible, or you will surely lose."
    i "When there are two or more, just fight normally."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNo matter how many zombies there are, cut them down."

    return


label frederika_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Enjoying having sex with a corpse...{w}\nHow miserable."

    show ilias st01 with dissolve #700

    i "Frederika is quite powerful, and her moves are unavoidable."
    i "Save your SP for recovery, and always try to keep more than half of your health."
    i "It may be a long fight, but you must be careful."
    i "If you are bound and about to be violated, you must {i}Attack{/i} to deal with it."
    i "If you choose to {i}Struggle{/i} instead, you will surely be raped."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMercy is not needed for a zombie."

    return


label chrom_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by a necromancer...{w}\nI'm too disgusted to even try to say something witty."

    show ilias st01 with dissolve #700

    i "Though Chrome has low HP, she is quite powerful.{w}\nShe also wields some troubling techniques..."
    i "Even the names of them are pretty embarrassing..."
    i "If she gives you an injection, you will take major damage and become paralyzed.{w}\nIn addition, she can still bind you while you're paralyzed."
    i "When you're about to be violated, you have to {i}Attack{/i} to get her off."
    i "Since her HP is low, you need to defeat her quickly.{w}\nBeing too defensive will lead to defeat..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSlam your iron hammer of justice on that evil scientist."

    return


label fairy_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Sheesh.{w} What kind of Hero gets beaten by a tiny Fairy?"

    show ilias st01 with dissolve #700

    i "The Fairy is very weak.{w}\nEven her embarrassing binding move just needs to be peeled off."
    i "But she does have a higher avoidance rate.{w}\nUse your skills as much as possible to make sure you hit."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nWhy don't you try punishing that trickster with your new technique?"

    return


label elf_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Violated by an Elf...{w} Are you going to quit your adventure now?"
    i "Is your only purpose on this journey to end up as a monster's prey?"

    show ilias st01 with dissolve #700

    i "The Elf will take her time to fire off a paralyzing arrow.{w}\nOnce you're hit, she will approach and attack."
    i "Once you recover, she'll take her distance and start over.{w}\nEven though she's a monster, she has a strategy."
    i "When she's away from you, you won't be able to hit her.{w}\nBut any skill that you have that closes the distance between the two of you quickly will hit."
    i "You can also hit her right after recovering from the paralysis.{w}\nYou need to time your attacks well."
    i "In addition, the Elf may throw you down and try to violate you.{w}\nIf this happens, you need to struggle or attack her right away."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCut down that arrogant self-proclaimed \"Guardian of the Forest\"."

    return


label tfairy_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Made into the fairies toy...{w}\nWhat a pathetic Hero!"

    show ilias st01 with dissolve #700

    i "Even though they're weak, facing two of them can be troublesome."
    i "Since they're fairies, their avoidance is also annoyingly high.{w}\nIf you want to ensure you hit, you must use your skills."
    i "But you'll need to keep enough SP for recovery, so be careful.{w}\nIt's dangerous when they grab on to you, so peel them off at once!"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEradicate those fairy pests with your sword of justice!"

    return


label fairys_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "No matter what, defeat isn't allowed for a Hero.{w}\nIt looks like you don't understand this."

    show ilias st01 with dissolve #700

    i "When fairies collect like flies, they become threatening.{w}\nThis fight may be rather difficult..."
    i "You may have no choice but to attack slowly as you use SP for recovery."
    i "It's also extremely dangerous when you're bound.{w}\nIt may be too late already, but you must struggle right away."
    i "In addition, it's possible to avoid this fight altogether.{w}\nIf you can't win no matter what you try, it's always a choice to skip it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThe interior of the forest is only a small ways ahead..."

    return


label sylph_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Is blindly rushing forward all you can do?{w}\nA stupid hero losing to a stupid monster. Bah!"

    show ilias st01 with dissolve #700

    i "There are two important things to know about this fight.{w}\nIf you attack Sylph when her wall is up, she will use a devastating counterattack."
    i "The other important thing...{w}\nSylph's defensive wall doesn't last forever."
    i "If you keep those two in mind, the method to win should be clear.{w}\nEven the dumbest of Heroes can figure it out."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBring harm to that strange spirit."

    return


label c_dryad_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "..................."

    show ilias st01 with dissolve #700

    i "Your opponent is very powerful, so you must fight carefully.{w}\nIt would be wise to save your SP for recovery."
    i "It's extremely dangerous when you're restrained, so you must break free right away."
    i "If the Chimera Dryad uses {i}Floral Funeral Preparations{/i} while you're bound, you have to attack right away to interrupt it."
    i "There's a small chance that it can put you in a trance, but there's no way to stop it, so keep your HP high."
    i "Lastly, you must avoid the {i}Rose of Strangulation{/i} at all costs.{w}\nYou get a warning before it's used, so use that to prepare."
    i "If you decrease the enemy's HP a certain amount, you may awaken a new power as well..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNo matter how powerful the enemy, never yield!"

    return


label taran_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Did you enjoy becoming a large spider's food?{w}\nBeing raped like that... Are you happy with that outcome?"

    show ilias st01 with dissolve #700

    i "Her most dangerous attack is the {i}Viscous Silk Spray{/i}{w}\nOnce you take this attack, your fate is sealed."
    i "The only way to stop it is to summon Sylph and create a wall of wind.{w}\nIf you do that, you can blow away her attack..."
    i "...Even if she's a spirit, she's still a monster.{w}\nIt is outrageous to have to rely on her power..."
    i "But if you use it to crush monsters, I'll tolerate it.{w}\nHandle her like a rag, and throw her away when she's no longer useful."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThe new land is waiting for your assistance..."

    return


label mino_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Defeated by an enemy whose only feature is her muscles...{w}\nDon't you feel pathetic being endlessly milked by a cow?"

    show ilias st01 with dissolve #700

    i "Her most dangerous attack is with that giant ax...{w}\nIf she hits you with it, you're sure to lose."
    i "But you can nullify it with your wind.{w}\nAt the beginning of combat, be sure to protect yourself with it..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't allow a monster with muscles for a brain defeat you..."

    return


label sasori_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "You were made into the Scorpion Girl's food? How miserable...{w}\nDid you enjoy being sucked dry?"

    show ilias st01 with dissolve #700

    i "She only has two annoying moves.{w}\nHer {i}Venom Tail{/i} and {i}Petra Scorpio{/i}."
    i "You can nullify her tail with Sylph.{w}\nAs for the other, you need to struggle as soon as she climbs on top of you."
    i "If you delay for even a turn, you will be raped.{w}\nIf you are paralyzed when this happens... Well, you can guess at the result."
    i "Another tactic could be to overwhelm her with your skills before she has a chance to counter attack."
    i "A little bit riskier, but it may work out."
    i "The chance of a quick loss is high, so choose wisely...{w}\nThe safest plan may be to use Sylph."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't be defeated by the heat, or an insect..."

    return


label lamp_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Seduced so easily like that...{w}\nI'm so frustrated, I don't even really want to talk to you."

    show ilias st01 with dissolve #700

    i "Her most frightening move is the {i}Heavenly Kamasutra{/i}.{w}\nIf she rapes you with that, your fate will be sealed."
    i "But if you call Sylph, you can prevent this technique.{w}\nIf you want to live, be sure to summon Sylph."
    i "In addition, her {i}Fingers of Ecstasy{/i} can put you into a trance.{w}\nThough not guaranteed, Sylph may be able to block this too."
    i "Her HP is rather high, so a blitz attack may not be wise.{w}\nJust overwork Sylph, and have a careful battle."
    i "She's a tough enemy, but it's possible to avoid her.{w}\nIf you can't win no matter what you try, you can always choose that path."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must drink water to prevent heat stroke!"

    return


label mummy_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Squeezed as dry as a dried fish...{w}\nShall I call you {i}Dried Fish Hero{/i} from now on?"

    show ilias st01 with dissolve #700

    i "First of all, Sara is useless.{w}\nPretend like she isn't even there."
    i "Sylph is very useful against someone like the Mummy Girl.{w}\nSince many of her attacks are bandage based, the wind can cut them up."
    i "In addition, you must prevent the {i}Mummy Package{/i} technique at all costs."
    i "If you take the attack, you will surely lose.{w}\nSylph can protect you against it."
    i "Work her as hard as you can, until she becomes worn out."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\n...A trial is meant for a god to test the spirit of man."
    i "But those monsters in the Pyramid are trying to give a trial as if they were gods..."
    i "Give conceited monsters like that your judgement."

    return


label kobura_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "...You're here again?{w}\nSheesh, this has become too frequent..."

    show ilias st01 with dissolve #700

    i "As usual, Sara is useless.{w}\nPretend as if she isn't there."
    i "When bound by the Cobra Girl, she will use {i}Cobra Anal{/i}.{w}\nIf you take this attack, you will be defeated. Be careful."
    i "If you call Sylph beforehand, it's likely she will prevent it.{w}\nIn addition, her paralysis attack can be nullified by Sylph."
    i "If you wish to be safe, you should call Sylph right away."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEradicate the monsters who lurk in the Pyramid."

    return


label lamias_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Was it fun to be played with by a group of Lamias?{w}\nGiven your record, I wouldn't be surprised if you would have rather been eaten..."

    show ilias st01 with dissolve #700

    i "Now you don't even have to pretend like Sara isn't there.{w}\n...It's not like she's useful, anyway."
    i "You will take continuous damage in this battle, so don't neglect your health, especially after being restrained."
    i "Sylph isn't particularly effective here...{w}\nBut it may be more efficient to use your SP to end the battle quickly."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou're almost to the deepest part of the Pyramid..."

    return


label sphinx_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "As expected, you came here.{w}\nAs soon as you answered her riddle like that, I knew I would be seeing you."

    show ilias st01 with dissolve #700

    i "You cannot win this fight.{w}\nEven if you bring her health to zero, she will revive."
    i "Sphinx uses a variety of nasty abilities."
    i "{i}Isis's Lovemaking{/i} will both damage and put you in a trance.{w}\nSylph can block the attack, so be sure sure to use her."
    i "{i}Kiss of Death{/i} will kill you in a single blow.{w}\nSylph cannot block it, so you must protect yourself as she charges up."
    i "If you reduce her HP to zero, she may use a new ability..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYour only choice to continue your quest is to avoid this fight..."

    return


label suckvore_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Were you satisfied being sucked by even a strange creature like that?{w}\nYou're quite the miserable pervert."

    show ilias st01 with dissolve #700

    i "The Suck Vore has a troublesome restraint attack.{w}\nNot only is it damaging, but it heals her too."
    i "You may want to call Sylph to mitigate some damage.{w}\nAlong the way, try to use your SP on strong offensive skills."
    i "But you may need to save enough to recover once or twice...{w}\nIt seems as though your SP may be spread a bit thin..."
    i "But if you adapt to the situation and do your best, you should be fine."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBring your justice to the woman who's a plague on her own village."

    return


label wormv_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Losing to a villager... What a miserable Hero.{w}\nDo you really want to be pleasured by monsters that badly?"

    show ilias st01 with dissolve #700

    i "The Worm Villager is almost the same as the Suck Vore.{w}\nHowever, her restriction attack is stronger."
    i "Her attacks are pretty powerful, and she can drain your energy.{w}\nSince she can recover her HP, you may not want to drag this fight on too long."
    i "You may want to try raising your defense with Sylph and going on the attack."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't allow any more victims like her to be created."

    return


label ironmaiden_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Did you enjoy being tortured with pleasure?{w}\nIf you liked it so much, do you want me to make something similar for you?"

    show ilias st01 with dissolve #700

    i "The Iron Maiden will wait for a moment to counterattack.{w}\nIf she is ever able to counter, your defeat is assured.{w}\nTherefore, you must never attack when her lid is open."
    i "To complicate things, she can confuse you.{w}\nIf you're confused, you may attack her even when the lid is open..."
    i "You must have Sylph's protection to guard you, or you risk losing.{w}\nWhile ensuring you never lose her protection, just be patient and slowly whittle down her health."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't allow a torture device like that to live."

    return


label lily_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st02 with dissolve #700

    i "Did you enjoy living as her guinea pig?{w}\nThe next world, and the one after that... Do you want to keep living as a guinea pig?"

    show ilias st01 with dissolve #700

    i "Lily is a very dangerous opponent.{w}\nHer attacks are powerful, and she can put you into a trance."
    i "Her restraining attack and her trance inducing attack can both be blocked by Sylph, so be sure to summon her."
    i "You may not have enough SP to spare on attack skills, so be sure to always have enough to recover in case of an emergency."
    i "The fight may be difficult because she can heal herself...{w}\nBut as long as you're careful and don't take too many risks, you should be fine."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the ringleader of evil and save the village!"

    return


label arizigoku_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Squeezed dry by the Antlion Girl?{w}\nHow disgraceful..."

    show ilias st01 with dissolve #700

    i "It isn't possible to escape from her quicksand.{w}\nThe only thing you can do is attack her."
    i "You have to defeat her before she sucks you in.{w}\nA single useless action will cause your defeat."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that monster who traps unsuspecting travelers..."

    return


label sandw_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Dried up again?{w}\nYou really like being left dry..."

    show ilias st01 with dissolve #700

    i "The Sandworm has a lot of HP, and her restraining attack is damaging.{w}\nIn addition, when you're in her mouth you can't escape by struggling."
    i "The only way to escape is to attack her and force her to spit you out."
    i "Sylph can block her mouth from sucking you up...{w}\nBut the probability is low, so I wouldn't depend on it."
    i "The best way to fight should be to use your SP on attack skills, and then recovering after you escape from her mouth."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy every desert monster that attacks unsuspecting travelers..."

    return


label gnome_hansei:
    $ persistent.count_hanseikai += 1

    if persistent.count_hanseikai == 50:
        jump hansei_h

    show ilias st04 with dissolve #700

    i "Even if it's called a great spirit, it's still a monster.{w}\nDon't you feel pathetic at being made into a little girl's toy?"

    show ilias st01 with dissolve #700

    i "Gnome is a powerful enemy, but Sylph is quite effective.{w}\nIf you wish to survive her onslaught, Sylph will be critical."
    i "In other words, force the spirits to fight each other."
    i "Although her HP is huge, you don't need to take it all away.{w}\nOnce you lower it enough, something may happen..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy Gnome, and put an end to the belief in animism."

    return


label hansei_h:
    show ilias st04 with dissolve #700
    play music "audio/bgm/ero1.ogg"
    stop music fadeout 1
    voice "audio/voice/ilias1_01a.ogg"

    i "Do you remember how many pieces of bread you have eaten so far?{w}\nI don't eat bread, but I remember how many times you have come here."

    voice "audio/voice/ilias1_01b.ogg"

    i "It's the fiftieth time... What a miserable Hero!"

    show ilias st01 with dissolve #700
    voice "audio/voice/ilias1_02.ogg"

    i "I shall humiliate you as punishment."
    "Ilias suddenly lowers my pants and underwear!"
    l "Eh...? Ilias? What are you doing...?"

    voice "audio/voice/ilias1_03.ogg"

    i "I said I'm going to humiliate you.{w}\nWhile I force you to come with my hand, reflect on your actions."
    l "A... No...{w}\nIlias... Doing something like that is..."

    show ilias ct01 at topright zorder 15 as ct #690
    show ilias st02 at xy(X=-100)
    with dissolve #700

    "Ilias's pure hand grips my penis.{w}\nHer soft, warm palm firmly holds onto me."
    l "Auu..."
    "I can feel her thin fingers holding on to me.{w}\nThe holy Ilias is tightly grasping my dirty part...{w}\nAs if being corrupted, the immoral act makes me excited."

    voice "audio/voice/ilias1_04.ogg"

    i "Now, I'll shame you..."

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"

    "Ilias starts to slowly move her hand.{w}\nHer thin fingers slowly move up and down as she loosely holds onto my penis."
    "A finger touches the tip as she continues to slowly pump me."
    l "Aaa... This isn't..."
    "Slowly moving, she's being very gentle.{w}\nIt's as if her pure white fingers are playing with my pink rod."
    "But even with that slow movement, the pleasure was incredible.{w}\nThis can't be... Is Ilias really playing with my penis...?"

    voice "audio/voice/ilias1_05.ogg"

    i "Does it feel good, Luka?{w}\nHowever if you're a Hero, you have to endure this..."
    l "Y...Yes... Ahh..."
    "I clench my teeth and try to silently endure.{w}\nBut the movements of her fingers seem to laugh at my effort."
    "Slowly moving up and down, the tip of her pinky strokes the back of my penis."
    "Another finger traces around the tip of my urethral opening, as pre-come starts to leak out."

    voice "audio/voice/ilias1_06.ogg"

    i "Your dirty juice is coming out...{w}\nSuch a shameful Hero."
    "One of Ilias's fingers wipes up the pre-come and spreads it over the tip of my penis."
    "The slippery feeling was enough to make me unconsciously thrust my waist up."
    l "Hauu...{w} I can't take much more..."

    voice "audio/voice/ilias1_07.ogg"

    i "Defeated by just this much? How miserable.{w}\nBut I won't go easy on you..."
    "Ilias's loose hand on my penis starts to increase her pumping speed as she tightens her grip."
    "The increased pressure and speed gives immediate pleasure."
    l "Aaa!{w} Incredible!"
    "Ilias's handjob forces me to the edge of ejaculation.{w}\nThe tingling of the impending orgasm starts from my penis and starts to flow through my body."
    "If this keeps up, I'm going to come on her hand...{w}\nThe hand of the sacred goddess... Making it dirty with my semen..."
    l "No... Ilias's hand... I'll make it dirty..."

    voice "audio/voice/ilias1_08.ogg"

    i "Luka... I'll make you come."
    "Ilias says in a firm tone.{w}\nHearing that, I realize that I'm allowed to come."
    "At the same time, she tightens her grip on my penis.{w}\nIncreasing her speed, she's trying to quickly force me to come."
    "I surrender my body and mind to Ilias as she finishes me off."
    l "Ahhh!!"
    "Held by the pure hand of the sacred goddess, I finally come."

    call syasei1 
    show ilias ct02 zorder 15 as ct #690
    show ilias bk01 at xy(X=-100) zorder 10 as bk #691
    call syasei2 

    l "Auuu..."
    "Throbbing, my semen shoots out and covers Ilias's hand.{w}\nHer pure white hand... Now dirty with my semen..."

    voice "audio/voice/ilias1_09.ogg"

    i "You made my hand dirty with your seed...{w}\nSuch a corrupt Hero, Luka."
    "Even though she says that, she continues to milk the rest of my semen out of my penis."
    "The pleasant pumping forces out the rest of my come still in my urethra."

    stop hseanwave fadeout 1
    voice "audio/voice/ilias1_10.ogg"

    i "Your shaming ends with this...{w}\nDid you reflect on your actions, Luka?"

    hide ct
    show ilias bk01 zorder 10 as bk #691
    show ilias st01 at center
    with dissolve #700

    "While saying that, Ilias releases my penis."
    l "Auuu..."
    "Ilias looks at her hand, covered in my semen.{w}\nFlooded with the lingering feelings of my orgasm, my strength is drained."

    $ renpy.end_replay

    i "Now go, oh brave Luka.{w}\nAfter learning from this experience, don't be defeated by another monster."

    return


