init python:
    def get_size(d):
        d = renpy.easy.displayable(d)
        w, h = renpy.render(d, 0, 0, 0, 0).get_size()
        w, h = int(round(w)), int(round(h))
        return w, h

    def get_girls(page, max_page=2):
        girls = (
            get_granberia_ng,
            get_queenharpy_ng,
            get_tamamo_ng,
            get_alma_ng,
            get_sylph_ng,
            get_gnome_ng,
            get_erubetie_ng,
            get_undine_ng,
            get_salamander_ng
            )
        start = page*max_page - max_page
        end = start + max_page
        s = []
        s0 = girls[start:end]
        for i in s0:
            s.append(i())

        s0 = []

        for girl in s:

            if girl.unlock:
                i_img, pos = girl.m_pose(girl.x0)[0]
                x, y = pos
                i_img = LiveCrop((-x, -y, 400, 600), i_img)
                s0.append((girl.pedia_name, i_img,  girl))
            else:
                s0.append(("", Null(), False))

        return s0

    def pf():
        renpy.transition(Fade(.3, 0, .3, color="#fff"), layer="screens")

    def get_info_list(info):
        l = []
        line = ""
        info = __(info)
        info = "\t"+info
        info = info.replace("\n", " \t")
        info = info.split(" ")

        for word in info:

            if "\t" in word or len("".join((line, " ", word))) > 33:

                l.append(line)

                if "\t" in word:
                    word = word.expandtabs(4)

                line = word
            else:
                line = "".join((line, " ", word))

        l.append(line)
        return l

screen pedia_new(enter):
    default page = 1
    predict False
    tag menu

    $ p_girls = get_girls(page)

    add "book"

    grid 2 1:

        spacing 0
        xfill True
        yfill True
        # at p_fade

        for nm, i_img, obj in p_girls:

            if nm:
                button:
                    background None
                    hover_foreground Solid("#f002")
                    padding (0, 0)
                    margin (0, 0)
                    activate_sound "audio/m_papier02.mp3"

                    action [Show("pedia_base_new", girl=obj), Function(pf)]

                    add i_img align (0, 0)
                    text nm:
                        style "pbn_name"
                        align (.5, 0)
                        yoffset 10
            else:
                text "???":
                    # at p_fade
                    size 70
                    align (.5, .5)
                    font "fonts/DS Goose.ttf"
                    color "#808080"
                    outlines [(2, "#000")]

    add "book_foreground"

    imagebutton:
        auto "back_%s"
        align (1.0, 1.0)
        margin (5, 5)
        activate_sound "audio/m_papier02.mp3"

        if enter == "gm":
            action ShowMenu()
        else:
            action Return()


    use pedia_pages(page, 128)

screen pedia_pages(page, max_page):
    $ start = page*2 - 2 + 1
    $ end = start + 1

    style_group "ppages"

    imagebutton:
        align (0, .5)
        auto "left_%s_n"
        margin (5, 5)
        activate_sound "audio/m_papier02.mp3"

        action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page)), Function(pf)]

    imagebutton:
        align (1.0, .5)
        auto "right_%s_n"
        margin (5, 5)
        activate_sound "audio/m_papier02.mp3"
        action [If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), Function(pf)]

    label "- [start] -":
        align (.25, .94)

    label "- [end] -":
        align (.75, .94)

    key "K_PAGEDOWN" action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))
    key "K_PAGEUP" action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))

style ppages_label:
    xfill False
    background None

style ppages_label_text:
    color "#fffa"
    outlines [(2, "#000e", 0, 0)]
    size 22

screen pedia_base_new(girl):
    on "hide" action [
        SetField(persistent, "bk1", 0),
        SetField(persistent, "bk2", 0),
        SetField(persistent, "bk3", 0),
        SetField(persistent, "bk4", 0),
        SetField(persistent, "bk5", 0),
        SetField(persistent, "bk6", 0),
        SetField(persistent, "bk7", 0),
        SetField(persistent, "bk8", 0),
        SetField(persistent, "pose", 1),
        SetField(persistent, "face", 1),
        SetField(persistent, "battle", 0),
        SetField(persistent, "echi", "")]

    modal True
    default page = 0
    $ scope = girl.scope
    $ scope["_game_menu_screen"] = "custom_gm"
    $ scope["monster_name"] = girl.name
    $ scope["_window_auto"] = True
    $ scope["addon_on"] = 0

    add "book"
    style_group "pbn"

    if page == 0:

        $ img_list = girl.m_pose(girl.x0)
        for IMG, POS in img_list:
            add IMG pos POS
        add "book_f" xalign 1.0

        frame:
            background None
            align (1.0, 1.0)
            xysize (400, 600)
            padding (20, 20)
            margin (0, 0)

            vbox:
                xfill True
                spacing 15

                text "[girl.pedia_name!t]":
                    style "pbn_name"
                    xalign .5

                textbutton "Info" action SetScreenVariable('page', 1)
                textbutton "Data" action SetScreenVariable('page', 2)
                textbutton "Pose" action Show("pose", girl=girl)
                textbutton "CG" action If(girl.zukan_ko == None or persistent.defeat[girl.zukan_ko] != 0,
                                            Function(renpy.call_in_new_context, girl.label_cg)
                                            )
                textbutton "Recall" action SetScreenVariable('page', 3)


    elif page == 1:
        frame:
            xfill True
            yfill True
            background None
            padding (0, 15, 0, 25)
            margin (0, 0)

            has hbox:
                spacing 20
            vbox:
                box_wrap True

                text "[girl.pedia_name!t]":
                    style "pbn_name"
                    xalign .5

                $ info_list = get_info_list(girl.info)

                for line in info_list:
                    label "[line]":
                        style "pbn_info"

    elif page == 2:
        grid 2 1:
            xfill True

            frame:
                padding (30, 15)
                background None
                has vbox:
                    xfill True
                text "[girl.pedia_name!t]":
                    style "pbn_name"
                    xalign .5

                text "Lv: [girl.lv]":
                    size 22
                    font 'fonts/DS Goose.ttf'
                    color '#000'
                    outlines [(1, '#f3ede180')]
                text _('Hp: [girl.hp]{#pedia_base}'):
                    size 22
                    font 'fonts/DS Goose.ttf'
                    color '#000'
                    outlines [(1, '#f3ede180')]

                if girl.zukan_ko:
                    $ var = persistent.defeat[girl.zukan_ko]

                    text "Times Defeated: [var]":
                        size 22
                        font 'fonts/DS Goose.ttf'
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    text "Defeated: None":
                        size 22
                        font 'fonts/DS Goose.ttf'
                        kerning -0.5
                        color '#000'
                        outlines [(1, '#f3ede180')]
            frame:
                padding (20, 15)
                background None

                has vbox:
                    xfill True

                if girl.skills is not None:
                    text "Skills:":
                        size 24
                        xalign .5
                        font 'fonts/DS Goose.ttf'
                        color '#000'
                        outlines [(1, '#f3ede180')]

                    for num, sk_name in girl.skills:
                        text "[sk_name!t]{#pedia_base}:":
                            size 22
                            font 'fonts/DS Goose.ttf'
                            color '#fffa'
                            outlines [(2, '#000a')]
                        text _("    Taken: %d, Orgasms: %d{#data}" % (persistent.skills[num][0], persistent.skills[num][1])):
                            size 22
                            font 'fonts/DS Goose.ttf'
                            color '#000'
                            outlines [(2, '#f3ede180')]

    elif page == 3:
        grid 2 1:
            xfill True

            frame:
                padding (30, 15)
                background None
                has vbox:
                    xfill True

                if girl.zukan_ko:
                    textbutton "Battle {b}NORMAL{/b}":
                        action [SetDict(scope, "Difficulty", 1),
                                SetDict(scope, "replay_h", False),
                                Replay(girl.label + "_start", scope=scope)]
                    if persistent.battle != 1:
                        textbutton "Battle {b}HARD{/b}":
                            action [SetDict(scope, "Difficulty", 2),
                                    SetDict(scope, "replay_h", False),
                                    Replay(girl.label + "_start", scope=scope)]
                        textbutton "Battle {b}HELL{/b}":
                            action [SetDict(scope, "Difficulty", 3),
                                    SetDict(scope, "replay_h", False),
                                    Replay(girl.label + "_start", scope=scope)]
            frame:
                padding (20, 15)
                background None

                has vbox:
                    xfill True

                if len(girl.echi) == 1:
                    if girl.echi[0]:
                        $ hlb = girl.echi[0]
                    else:
                        $ hlb = girl.label + "_h"

                    textbutton _("Rape Scene"):
                        action [SetDict(scope, "replay_h", True),
                                SetDict(scope, "monster_name", girl.name),
                                Replay(hlb, scope=scope)]
                else:
                    for num, replay_lb in enumerate(girl.echi, 1):

                        textbutton "Rape Scene [num]":
                            action [SetDict(scope, "replay_h", True),
                                    SetDict(scope, "monster_name", girl.name),
                                    Replay(replay_lb , scope=scope)]

    imagebutton:
        auto "back_%s"
        align (1.0, 1.0)
        margin (5, 5)
        activate_sound "audio/m_papier02.mp3"

        if page != 0:
            action SetScreenVariable('page', 0)
        else:
            action Hide("pedia_base_new")

style pbn_name:
    size 24
    font "fonts/DS Goose.ttf"
    color "#fff000"
    outlines [(2, "#000a", 0, 0)]

style pbn_info:
    is label
    margin (0, 0)
    padding (27, 0, 0, 0)
    xsize 400

style pbn_info_text:
    size 18
    font "fonts/DS Goose.ttf"
    color "#000"
    outlines [(1, "#fffa")]

style pbn_button:
    is button
    background None
    activate_sound "audio/m_papier02.mp3"

style pbn_button_text:
    size 24
    font "fonts/DS Goose.ttf"
    color "#fff"
    hover_color "#f00"
    insensitive_color "#8888"
    outlines [(2, "#000a")]
