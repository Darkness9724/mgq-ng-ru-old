init python:
    def get_colors():
        COLORS = [
        (0, 0, 0), (0, 0, 51), (0, 0, 102), (0, 0, 153), (0, 0, 204), (0, 0, 255),
        (0, 51, 0), (0, 51, 51), (0, 51, 102), (0, 51, 153), (0, 51, 204), (0, 51, 255),
        (0, 102, 0), (0, 102, 51), (0, 102, 102), (0, 102, 153), (0, 102, 204), (0, 102, 255),
        (0, 153, 0), (0, 153, 51), (0, 153, 102), (0, 153, 153), (0, 153, 204), (0, 153, 255),
        (0, 204, 0), (0, 204, 51), (0, 204, 102), (0, 204, 153), (0, 204, 204), (0, 204, 255),
        (0, 255, 0), (0, 255, 51), (0, 255, 102), (0, 255, 153), (0, 255, 204), (0, 255, 255),
        (51, 0, 0), (51, 0, 51), (51, 0, 102), (51, 0, 153), (51, 0, 204), (51, 0, 255),
        (51, 51, 0), (51, 51, 51), (51, 51, 102), (51, 51, 153), (51, 51, 204), (51, 51, 255),
        (51, 102, 0), (51, 102, 51), (51, 102, 102), (51, 102, 153), (51, 102, 204), (51, 102, 255),
        (51, 153, 0), (51, 153, 51), (51, 153, 102), (51, 153, 153), (51, 153, 204), (51, 153, 255),
        (51, 204, 0), (51, 204, 51), (51, 204, 102), (51, 204, 153), (51, 204, 204), (51, 204, 255),
        (51, 255, 0), (51, 255, 51), (51, 255, 102), (51, 255, 153), (51, 255, 204), (51, 255, 255),
        (102, 0, 0), (102, 0, 51), (102, 0, 102), (102, 0, 153), (102, 0, 204), (102, 0, 255),
        (102, 51, 0), (102, 51, 51), (102, 51, 102), (102, 51, 153), (102, 51, 204), (102, 51, 255),
        (102, 102, 0), (102, 102, 51), (102, 102, 102), (102, 102, 153), (102, 102, 204), (102, 102, 255),
        (102, 153, 0), (102, 153, 51), (102, 153, 102), (102, 153, 153), (102, 153, 204), (102, 153, 255),
        (102, 204, 0), (102, 204, 51), (102, 204, 102), (102, 204, 153), (102, 204, 204), (102, 204, 255),
        (102, 255, 0), (102, 255, 51), (102, 255, 102), (102, 255, 153), (102, 255, 204), (102, 255, 255),
        (153, 0, 0), (153, 0, 51), (153, 0, 102), (153, 0, 153), (153, 0, 204), (153, 0, 255),
        (153, 51, 0), (153, 51, 51), (153, 51, 102), (153, 51, 153), (153, 51, 204), (153, 51, 255),
        (153, 102, 0), (153, 102, 51), (153, 102, 102), (153, 102, 153), (153, 102, 204), (153, 102, 255),
        (153, 153, 0), (153, 153, 51), (153, 153, 102), (153, 153, 153), (153, 153, 204), (153, 153, 255),
        (153, 204, 0), (153, 204, 51), (153, 204, 102), (153, 204, 153), (153, 204, 204), (153, 204, 255),
        (153, 255, 0), (153, 255, 51), (153, 255, 102), (153, 255, 153), (153, 255, 204), (153, 255, 255),
        (204, 0, 0), (204, 0, 51), (204, 0, 102), (204, 0, 153), (204, 0, 204), (204, 0, 255),
        (204, 51, 0), (204, 51, 51), (204, 51, 102), (204, 51, 153), (204, 51, 204), (204, 51, 255),
        (204, 102, 0), (204, 102, 51), (204, 102, 102), (204, 102, 153), (204, 102, 204), (204, 102, 255),
        (204, 153, 0), (204, 153, 51), (204, 153, 102), (204, 153, 153), (204, 153, 204), (204, 153, 255),
        (204, 204, 0), (204, 204, 51), (204, 204, 102), (204, 204, 153), (204, 204, 204), (204, 204, 255),
        (204, 255, 0), (204, 255, 51), (204, 255, 102), (204, 255, 153), (204, 255, 204), (204, 255, 255),
        (255, 0, 0), (255, 0, 51), (255, 0, 102), (255, 0, 153), (255, 0, 204), (255, 0, 255),
        (255, 51, 0), (255, 51, 51), (255, 51, 102), (255, 51, 153), (255, 51, 204), (255, 51, 255),
        (255, 102, 0), (255, 102, 51), (255, 102, 102), (255, 102, 153), (255, 102, 204), (255, 102, 255),
        (255, 153, 0), (255, 153, 51), (255, 153, 102), (255, 153, 153), (255, 153, 204), (255, 153, 255),
        (255, 204, 0), (255, 204, 51), (255, 204, 102), (255, 204, 153), (255, 204, 204), (255, 204, 255),
        (255, 255, 0), (255, 255, 51), (255, 255, 102), (255, 255, 153), (255, 255, 204), (255, 255, 255)]

        c_img = [[c + (255,),
                Solid(c + (255,), xysize=(15, 15)),
                LiveComposite((15, 15),
                    (0, 0), Solid("#000", xysize=(15, 15)),
                    (2, 2), Solid(c + (255,), xysize=(11, 11)))
            ] for c in COLORS]
        return c_img

image separate = Solid("#fff", xysize=(700, 2))


###########################################################################
#
#
#
###########################################################################
screen c_styles(enter="st"):
    default show_screen = "bg"
    default c_img = get_colors()
    tag menu

    if enter == 'mm':
        add "bg 001"
    else:
        add renpy.get_screen("bg", layer='master')

    add "gm_bg"

    frame:
        background Frame("images/System/win_bg_0.webp", 5, 5)
        xfill True
        yfill True
        margin (30, 60)

        vbox:
            style_group "st"
            spacing 5

            if show_screen == "bg":
                frame:
                    has vbox
                    label "Настройка окна"
                    add "separate" xalign .5
                    null height 5
                    use choice_color('box', c_img)
                    null height 5
                    add "separate" xalign .5
            else:
                textbutton "Настройка окна" action SetScreenVariable(name='show_screen', value="bg")


            if show_screen == "fnt":
                frame:
                    has vbox
                    add "separate" xalign .5
                    label "Настройка шрифта"
                    add "separate" xalign .5
                    null height 5
                    use fnt_scr
                    null height 5
                    add "separate" xalign .5
            else:
                textbutton "Настройка шрифта" action SetScreenVariable(name='show_screen', value="fnt")


            if show_screen == "txt":
                frame:
                    has vbox
                    add "separate" xalign .5
                    label "Настройка текста"
                    add "separate" xalign .5
                    null height 5
                    use choice_color('what', c_img)
                    null height 5
                    add "separate" xalign .5
            else:
                textbutton "Настройка текста" action SetScreenVariable(name='show_screen', value="txt")


            if show_screen == "nm":
                frame:
                    has vbox
                    add "separate" xalign .5
                    label "Настройка имени"
                    add "separate" xalign .5
                    null height 5
                    use choice_color('who', c_img)
                    # add "separate" xalign .5
            else:
                textbutton "Настройка имени" action SetScreenVariable(name='show_screen', value="nm")
                # add "separate" xalign .5

        use test_window

    use title(_("Настройка стиля"))
    use return_but(enter)

style st_label:
    xalign .5

style st_label_text:
    outlines [(2, "#000")]

style st_frame:
    # xysize (700,
    ymaximum 260
    xfill True
    padding (10, 0)
    xalign .5
    background None

style st_label_text:
    size 24
    font "fonts/archiform.otf"
    color '#ffcc33'

style st_button:
    background None
    xalign .5

style st_button_text:
    is gm_button_text
    size 22
    idle_color "#fff"
    hover_color "#f00"
    insensitive_color "#fff"
    insensitive_outlines [(2, "#303030")]


###########################################################################
#
#
#
###########################################################################
screen choice_color(param="", c_img):

    vpgrid:
        xfill True
        draggable True
        mousewheel True
        scrollbars "vertical"
        rows 1
        style_group "st_txt"

        vbox:
            label "Цвет текста:"

            grid 36 6:
                xfill True

                for COLOR, IDLE, HOVER in c_img:
                    imagebutton:
                        # xysize (14, 10)

                        idle IDLE
                        hover HOVER
                        selected HOVER
                        focus_mask True

                        action SetField(persistent, '%s_color' % param, COLOR)

            if param != 'box':

                label "Цвет обводки:"

                grid 36 6:
                    xfill True

                    for COLOR, IDLE, HOVER in c_img:
                        imagebutton:
                            # xysize (14, 10)

                            idle IDLE
                            hover HOVER
                            selected HOVER
                            focus_mask True

                            action If(getattr(persistent, '%s_out_thickness' % param), SetField(persistent, '%s_out_color' % param, COLOR))

                label "Толщина обводки:"

                hbox:
                    textbutton "1 px" action SetField(persistent, '%s_out_thickness' % param, 1)
                    textbutton "2 px" action SetField(persistent, '%s_out_thickness' % param, 2)
                    textbutton "None" action SetField(persistent, '%s_out_thickness' % param, 0)
            else:
                label "Фон:"

                hbox:
                    spacing 5

                    for num, box in enumerate(bgs):
                        textbutton "%d" % (num+1):
                            action If(persistent.box != box, SetField(persistent, "box", box))

style st_txt_vscrollbar:
    is pref_vscrollbar

style st_txt_label:
    is label
    top_margin 10
    xalign 0

style st_txt_label_text:
    is label_text
    color "#fffa"
    outlines [(2, "#000a")]
    font "fonts/archiform.otf"
    size 19

style st_txt_button:
    background None
    margin (0, 0)
    padding (0, 0)

style st_txt_button_text:
    is gm_button_text
    size 18
    idle_color "#fff"
    hover_color "#f00"
    outlines [(2, "#000a")]
    insensitive_color "#888a"
    insensitive_outlines [(2, "#303030")]


###########################################################################
#
#
#
###########################################################################
screen fnt_scr():

    vpgrid:
        spacing 5
        draggable True
        mousewheel True
        xfill True
        scrollbars "vertical"
        rows 16
        style_group "fnt"

        textbutton "Arial Unicode Ms" action If(persistent.main_font_name != "arial", Function(SetCustomFont, arial_fnt))
        textbutton "Blogger Sans" action If(persistent.main_font_name != "blogger", Function(SetCustomFont, blogger_fnt))
        textbutton "Comic Sans MS" action If(persistent.main_font_name != "comic_sans", Function(SetCustomFont, comic_sans_fnt))
        textbutton "Consolas" action If(persistent.main_font_name != "consolas", Function(SetCustomFont, consolas_fnt))
        textbutton "Georgia" action If(persistent.main_font_name != "georgia", Function(SetCustomFont, georgia_fnt))

        textbutton "Helvetica" action If(persistent.main_font_name != "helvetica", Function(SetCustomFont, helvetica_fnt))
        textbutton "Lucida Sans Unicode" action If(persistent.main_font_name != "lucida", Function(SetCustomFont, lucida_fnt))
        textbutton "Medieval English Normal" action If(persistent.main_font_name != "medieval", Function(SetCustomFont, medieval_fnt))
        textbutton "Ms Gothic" action If(persistent.main_font_name != "msgothic", Function(SetCustomFont, msgothic_fnt))
        textbutton "Ms Mincho" action If(persistent.main_font_name != "msmincho", Function(SetCustomFont, msmincho_fnt))

        textbutton "Papyrus-Ru" action If(persistent.main_font_name != "papyrus", Function(SetCustomFont, papyrus_fnt))
        textbutton "President Cyr" action If(persistent.main_font_name != "president", Function(SetCustomFont, president_fnt))
        textbutton "Kz Taurus" action If(persistent.main_font_name != "taurus", Function(SetCustomFont, taurus_fnt))
        textbutton "Times New Roman" action If(persistent.main_font_name != "times", Function(SetCustomFont, times_fnt))
        textbutton "Vera Humana 95" action If(persistent.main_font_name != "vera",Function(SetCustomFont, vera_fnt))

        textbutton "Verdana" action If(persistent.main_font_name != "verdana", Function(SetCustomFont, verdana_fnt))


style fnt_vscrollbar:
    is pref_vscrollbar

style fnt_button:
    background None

style fnt_button_text:
    idle_color "#fff"
    hover_color "#000fff"
    selected_color "#888"
    size 18
    outlines [(1, "000c")]


init python:
    class CustomFont():
        def __init__(self, name, size, kerning=1, leading=0, spacing=0, bold=False, italic=False):
            self.path = "".join(["fonts/", name, ".ttf"])
            self.name = name
            self.size = size
            self.kerning = kerning
            self.leading = leading
            self.spacing = spacing
            self.bold = bold
            self.italic = italic

    arial_fnt = CustomFont("arialuni", size=20)
    blogger_fnt = CustomFont("blogger", size=20)
    comic_sans_fnt = CustomFont("comic_sans", size=20)
    consolas_fnt = CustomFont("consolas", size=20)
    georgia_fnt = CustomFont("georgia", size=20)
    helvetica_fnt = CustomFont("helvetica", size=20)
    lucida_fnt = CustomFont("lucida", size=20)
    medieval_fnt = CustomFont("medieval", size=20)
    msgothic_fnt = CustomFont("msgothic", size=20)
    msmincho_fnt = CustomFont("msmincho", size=20)
    papyrus_fnt = CustomFont("papyrus", size=20)
    president_fnt = CustomFont("president", size=20)
    taurus_fnt = CustomFont("taurus", size=20)
    times_fnt = CustomFont("times", size=20)
    vera_fnt = CustomFont("vera", size=20)
    verdana_fnt = CustomFont("verdana", size=20)

    def SetCustomFont(obj):
        persistent.main_font_name = obj.name
        store.main_font = obj

    if persistent.main_font_name is None:
        SetCustomFont(verdana_fnt)
    elif persistent.main_font_name == "arial":
        SetCustomFont(arial_fnt)
    elif persistent.main_font_name == "blogger":
        SetCustomFont(blogger_fnt)
    elif persistent.main_font_name == "comic_sans":
        SetCustomFont(comic_sans_fnt)
    elif persistent.main_font_name == "consolas":
        SetCustomFont(consolas_fnt)
    elif persistent.main_font_name == "georgia":
        SetCustomFont(georgia_fnt)
    elif persistent.main_font_name == "helvetica":
        SetCustomFont(helvetica_fnt)
    elif persistent.main_font_name == "lucida":
        SetCustomFont(lucida_fnt)
    elif persistent.main_font_name == "medieval":
        SetCustomFont(medieval_fnt)
    elif persistent.main_font_name == "msgothic":
        SetCustomFont(msgothic_fnt)
    elif persistent.main_font_name == "msmincho":
        SetCustomFont(msmincho_fnt)
    elif persistent.main_font_name == "papyrus":
        SetCustomFont(papyrus_fnt)
    elif persistent.main_font_name == "president":
        SetCustomFont(president_fnt)
    elif persistent.main_font_name == "taurus":
        SetCustomFont(taurus_fnt)
    elif persistent.main_font_name == "times":
        SetCustomFont(times_fnt)
    elif persistent.main_font_name == "vera":
        SetCustomFont(vera_fnt)
    elif persistent.main_font_name == "verdana":
        SetCustomFont(verdana_fnt)


##############################################################################
# Test Window
#
# A screen that's included by the default say screen, and adds quick access to
# several useful functions.
screen test_window():
    vbox:
        style "say_two_window_vbox"
        align (.5, 1.0)

        add "separate" xalign .5

        window:
            background Frame(im.MatrixColor(persistent.box,
                                   im.matrix.colorize(persistent.box_color, persistent.box_color)), 12, 12, tile=True
                                )
            margin (5, 5, 5, 1)
            padding (8, 5)
            xminimum 250

            style "say_who_window"

            text "Имя":
                xalign .5
                font persistent.who_font
                color persistent.who_color
                size persistent.what_font_size
                outlines [(persistent.who_out_thickness, persistent.who_out_color, 0, 0),
                          (persistent.who_out_thickness, "#0003", 2, 2)]

        window:
            id "window"
            background Frame(im.MatrixColor(persistent.box,
                                   im.matrix.colorize(persistent.box_color, persistent.box_color)), 12, 12, tile=True
                                )

            ysize 40
            xpadding 10
            xmargin 5

            has vbox:
                style "say_vbox"

            text "Съешь ещё этих мягких французских булок, да выпей чаю":
                font persistent.what_font
                size persistent.what_font_size
                color persistent.what_color
                line_spacing persistent.what_out_thickness
                line_leading persistent.what_out_thickness
                outlines [(persistent.what_out_thickness, persistent.what_out_color, 0, 0),
                              (persistent.what_out_thickness, "#0003", 2, 2)]

style aqua_text:
    color "#00BFFF"
    selected_color "#ff0"
