####################################################
#
# Transition
#
####################################################

# define close_eyes = ImageDissolve('images/System/eye.webp', 2, ramplen=64)
# define open_eyes = ImageDissolve('images/System/eye.webp', 2, reverse=True, ramplen=64)
define blinds = ImageDissolve('images/System/blinds.webp', 2.0)
define blinds_r = ImageDissolve(im.Flip('images/System/blinds.webp', horizontal=True), 2.0)
define crash = ImageDissolve('images/System/crash.webp', 4.5)
define blinds2 = MultipleTransition([False, blinds, '#000', blinds, True])
define blinds2_r = MultipleTransition([False, blinds_r, '#000', blinds_r, True])

# define quake = Shake((0, 0, 0, 0), 2.0, dist=25)
# define quake2 = Shake((0, 0, 0, 0), 2.0, dist=30)

define syasei1 = Fade(0.3, 0.5, 0.3, color="#fff")
define syasei2 = Fade(0.2, 0.2, 0.2, color="#fff")
define flash = Fade(0.3, 0, 0.3, color="#fff")
define flash2 = Fade(0.15, 0.5, 0.15, color="#fff")

####################################################
#
# Transforms
#
####################################################
transform lightwave(dt=.25, z=1.5):
    truecenter zoom 1.0
    easeout dt*.5 truecenter zoom z
    easein dt*.5 truecenter zoom 1.0
    repeat 1

transform sk:
    on show:
        alpha 0
        align (.5, 0.05)
        xoffset -60
        linear 0.5 xoffset 0 alpha 1.0

    on hide:
        alpha 1.0
        xoffset 0
        linear 0.5 xoffset 60 alpha 0

transform shake(dt=.4, dist=128):
    function renpy.curry(_shake_function)(dt=dt, dist=dist)

transform xshake(new_widget, old_widget, dt=.4, dist=256):
    delay dt
    # truecenter

    contains:
        new_widget
        truecenter
        function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="x")

    contains:
        old_widget
        truecenter
        alpha 0
        # function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="x")

transform yshake(dt=.4, dist=128):
    function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="y")

transform slash:
    ease .1 alpha 0
    ease .1 alpha 1.0
    repeat 3

transform xyzoom(factor):
    zoom factor

transform alpha_off:
    alpha 1.0

transform xy(X=0, Y=0):
    anchor (0, 0)
    pos (X, Y)

transform miss:
    subpixel True
    linear 0.25 xoffset 80
    linear 0.25 xoffset 0

transform miss2:
    subpixel True
    zoom 1.25
    linear 0.25 xoffset 80
    linear 0.25 xoffset 0
    zoom 1.0

transform maou_cruelty:
    truecenter

    parallel:
        ease 2.0 zoom 32.0
    parallel:
        "#ffffff" with Dissolve(2.0, alpha=False)

transform syoukan_effect:
    subpixel True
    anchor (.5, .5)
    pos (0, 300)
    alpha 0.0
    easein 1.7 xpos 400 alpha 1.0 zoom 1.1
    easeout 1.7 xpos 800 alpha 0 zoom 1.0

transform syoukan_effect2:
    subpixel True
    alpha 1.0
    xcenter 0
    zoom 0.9
    easein 1.7 xcenter 400 zoom 1.0
    easeout 1.7 xcenter 800 zoom 0.9

transform flash_but:
    alpha 0
    .4
    alpha 1.0
    .4
    repeat

transform appear(t):
    delay t
    alpha 0.0
    subpixel True
    pause t
    linear .5 alpha 1.0


####################################################
#
# Animation
#
####################################################
image demon decapitation2 = Movie(channel="effects", play="video/dd.webm", mask="video/dd_mask.webm", loops=0)

image demon decapitation:
    "images/Effects/009_01.webp"
    pause .05
    "images/Effects/009_02.webp"
    pause .05
    "images/Effects/009_03.webp"
    pause .05
    "images/Effects/009_04.webp"
    pause .05
    "images/Effects/009_05.webp"
    pause .05

image thunder thrust a:
    "images/Effects/009a_01.webp"
    0.05
    "images/Effects/009a_02.webp"
    0.05
    "images/Effects/009a_03.webp"
    0.05
    "images/Effects/009a_04.webp"
    0.05
    "images/Effects/009a_05.webp"
    0.05

image thunder thrust b:
    "images/Effects/009b_01.webp"
    0.05
    "images/Effects/009b_02.webp"
    0.05
    "images/Effects/009b_03.webp"
    0.05
    "images/Effects/009b_04.webp"
    0.05
    "images/Effects/009b_05.webp"
    0.05

image spica:
    "images/Effects/029_01.webp"
    0.15
    "images/Effects/029_02.webp"
    0.15
    function play_earth1
    "images/Effects/029_03.webp"
    0.15
    "images/Effects/029_04.webp"
    0.15
    function play_fire2
    "images/Effects/029_05.webp"
    0.15
    "images/Effects/029_06.webp"
    0.15
    function play_wind2
    "images/Effects/029_07.webp"
    0.15
    "images/Effects/029_08.webp"
    0.15

image effect 010:
    "images/Effects/010_01.webp"
    0.05
    "images/Effects/010_02.webp"
    0.05
    "images/Effects/010_03.webp"
    0.05
    "images/Effects/010_04.webp"
    0.05

image effect 011:
    "images/Effects/011_01.webp"
    0.1
    "images/Effects/011_02.webp"
    0.1
    "images/Effects/011_03.webp"
    0.1
    "images/Effects/011_04.webp"
    0.1
    "images/Effects/011_05.webp"
    0.1

image effect 012:
    "images/Effects/012_01.webp"
    0.1
    "images/Effects/012_02.webp"
    0.1
    "images/Effects/012_03.webp"
    0.1
    "images/Effects/012_04.webp"
    0.1
    "images/Effects/012_05.webp"
    0.1
    "images/Effects/012_06.webp"
    0.1

image effect 014:
    "images/Effects/014_01.webp"
    0.1
    "images/Effects/014_02.webp"
    0.1
    "images/Effects/014_03.webp"
    0.1
    "images/Effects/014_04.webp"
    0.1
    "images/Effects/014_05.webp"
    0.1
    "images/Effects/014_06.webp"
    0.1
    "images/Effects/014_07.webp"
    0.1
    "images/Effects/014_08.webp"
    0.1
    "images/Effects/014_09.webp"
    0.1
    "images/Effects/014_10.webp"
    0.1
    "images/Effects/014_11.webp"
    0.1
    "images/Effects/014_12.webp"
    0.1
    "images/Effects/014_13.webp"
    0.1
    "images/Effects/014_14.webp"
    0.1

image effect 015:
    "images/Effects/015_01.webp"
    0.05
    "images/Effects/015_02.webp"
    0.05
    "images/Effects/015_03.webp"
    0.05
    "images/Effects/015_04.webp"
    0.05

image effect 016:
    "images/Effects/016_01.webp"
    0.05
    "images/Effects/016_02.webp"
    0.05
    "images/Effects/016_03.webp"
    0.05
    "images/Effects/016_04.webp"
    0.05
    "images/Effects/016_05.webp"
    0.05

image effect 017:
    "images/Effects/017_01.webp"
    0.05
    "images/Effects/017_02.webp"
    0.05
    "images/Effects/017_03.webp"
    0.05
    "images/Effects/017_04.webp"
    0.05
    "images/Effects/017_05.webp"
    0.05

image effect 023:
    "images/Effects/023_01.webp"
    0.05
    "images/Effects/023_02.webp"
    0.05
    "images/Effects/023_03.webp"
    0.05
    "images/Effects/023_04.webp"
    0.05

image effect 024:
    function play_slash5
    "images/Effects/024_01.webp"
    0.15
    "images/Effects/024_02.webp"
    0.15
    function play_slash5
    "images/Effects/024_03.webp"
    0.15
    "images/Effects/024_04.webp"
    0.15
    function play_slash5
    "images/Effects/024_05.webp"
    0.15
    "images/Effects/024_06.webp"
    0.15
    function play_slash5
    "images/Effects/024_07.webp"
    0.15
    "images/Effects/024_08.webp"
    0.15
    repeat 2

image effect 025:
    "images/effects/025_01.webp"
    0.1 #2
    "images/effects/025_02.webp"
    0.1 #2
    "images/effects/025_03.webp"
    0.1 #2
    function play_light
    "images/effects/025_04.webp"
    0.1 #2
    "images/effects/025_05.webp"
    0.1 #2
    "images/effects/025_06.webp"
    0.1 #2
    "images/effects/025_07.webp"
    0.1 #2
    "images/effects/025_08.webp"
    0.1


init -1 python:
    def play_light(trans, st, at):
        renpy.play("audio/se/light.ogg", channel="sound")

    def play_earth1(trans, st, at):
        renpy.play("audio/se/earth1.ogg", channel="audio")

    def play_fire2(trans, st, at):
        renpy.play("audio/se/fire2.ogg", channel="audio")

    def play_wind2(trans, st, at):
        renpy.play("audio/se/wind2.ogg", channel="audio")

    def play_slash(trans, st, at):
        renpy.play("audio/se/slash3.ogg", channel="audio")

    def play_slash5(trans, st, at):
        renpy.play("audio/se/slash5.ogg", channel="audio")

    def play_dageki(trans, st, at):
        renpy.play("audio/se/dageki.ogg", channel="sound")


image death sword chaos star:
    function play_slash
    "images/Effects/005_01.webp"
    0.125 #2
    function play_slash
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2
    function play_slash
    "images/Effects/005_01.webp"
    0.125 #2
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2
    "images/Effects/005_01.webp"
    0.125 #2
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2

image vaporizing rebellion sword:
    function play_slash
    function play_fire2
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08

image vaporizing rebellion sword typhoon:
    function play_slash
    function play_fire2
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "NGDATA/images/effect/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "NGDATA/images/effect/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    function play_slash
    "NGDATA/images/effect/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "NGDATA/images/effect/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08

image spirit fight:
    function play_dageki
    im.Composite((388 ,352),
        (10, 45), "images/Characters/sylph/sylph st21r.webp",
        (203, 0), "images/Characters/gnome/gnome st32r.webp")
    0.05
    im.Composite((388 ,352),
        (0, 41), "images/Characters/sylph/sylph st22r.webp",
        (108, 0), "images/Characters/gnome/gnome st33r.webp")
    0.05
    repeat 40


image rakshasa_new:
    0.125
    function play_slash
    "images/Effects/rakshasa_1.webp"
    0.125
    "images/Effects/rakshasa_2.webp"
    0.125
    "images/Effects/rakshasa_3.webp"
    0.125
    function play_slash
    "images/Effects/rakshasa_4.webp"
    0.125
    "images/Effects/rakshasa_5.webp"
    0.125
    "images/Effects/rakshasa_6.webp"
    0.125
    function play_slash
    "images/Effects/rakshasa_7.webp"
    0.125
    "images/Effects/rakshasa_8.webp"
    0.125
    "images/Effects/rakshasa_9.webp"
    repeat 3

image rakshasa:
    function play_slash
    "images/Effects/005_01.webp"
    0.04 #2
    "images/Effects/005_02.webp"
    0.04 #2
    "images/Effects/005_03.webp"
    0.04 #2
    "images/Effects/005_04.webp"
    0.04 #2
    repeat 9

####################################################
#
# Images with effects
#
####################################################
image effect 001 = "images/Effects/001.webp"
image effect 002 = "images/Effects/002.webp"
image effect 002 ng = "NGDATA/images/effect/002.webp"
image effect 002 red = im.MatrixColor("images/Effects/002.webp",
                                        im.matrix.desaturate() * im.matrix.tint(.75, .1, .1))

image effect 003 = "images/Effects/003.webp"
image effect 005_01 = "images/Effects/005_01.webp"
image effect 005_02 = "images/Effects/005_02.webp"
image effect 005_03 = "images/Effects/005_03.webp"
image effect 005_04 = "images/Effects/005_04.webp"

image effect 005_01 a = "images/Effects/005_01a.webp"
image effect 005_02 a = "images/Effects/005_02a.webp"
image effect 005_03 a = "images/Effects/005_03a.webp"
image effect 005_04 a = "images/Effects/005_04a.webp"

image effect 006 = "images/Effects/006.webp"
image effect 006a = "images/Effects/006a.webp"
image effect 007 = "images/Effects/007.webp"
image effect 008 = "images/Effects/008.webp"
image effect 013_01 = "images/Effects/013_01.webp"
image effect 013_02 = "images/Effects/013_02.webp"

image effect 018 = "images/Effects/018.webp"
image effect 019 = "images/Effects/019.webp"
image effect 020 = "images/Effects/020.webp"
image effect 021 = "images/Effects/021.webp"
image effect 022 = "images/Effects/022.webp"

image effect 029_01 = "images/Effects/029_01.webp"
image effect 029_02 = "images/Effects/029_02.webp"
image effect 029_03 = "images/Effects/029_03.webp"
image effect 029_04 = "images/Effects/029_04.webp"
image effect 029_05 = "images/Effects/029_05.webp"
image effect 029_06 = "images/Effects/029_06.webp"
image effect 029_07 = "images/Effects/029_07.webp"
image effect 029_08 = "images/Effects/029_08.webp"

image effect wind = "images/Effects/wind.webp"
image effect earth = "images/Effects/earth.webp"
image effect fire = "images/Effects/fire.webp"
image effect aqua = "images/Effects/aqua.webp"

# image m_circle1 = "images/Effects/m_circle1.webp"
image effect m_circle2 = "images/Effects/m_circle2.webp"
image effect thunder = "NGDATA/images/effect/thunder.webp"

init -99:
    image british_flag_wave:
        "images/Effects/british-flag/british-flag-small (0).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (1).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (2).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (3).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (4).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (5).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (6).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (7).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (8).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (9).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (10).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (11).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (12).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (13).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (14).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (15).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (10).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (11).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (12).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (13).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (14).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (15).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (16).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (17).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (18).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (19).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (20).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (21).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (22).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (23).webp"
        0.15
        "images/Effects/british-flag/british-flag-small (24).webp"
        0.15
        repeat

    image russian_flag_wave:
        "images/Effects/russian-flag/russian-flag-small (0).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (1).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (2).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (3).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (4).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (5).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (6).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (7).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (8).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (9).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (10).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (11).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (12).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (13).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (14).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (15).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (10).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (11).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (12).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (13).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (14).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (15).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (16).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (17).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (18).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (19).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (20).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (21).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (22).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (23).webp"
        0.15
        "images/Effects/russian-flag/russian-flag-small (24).webp"
        0.15
        repeat
