label mls_cmd_battle:

    $ zukanon = 3
    $ monster_name = mon_labo_mon_ini.data.name
    $ name2 = monster_name
    $ lavel = "tuika"
    $ tatie1 = mon_labo_mon_ini.data.tatie1

    if ".jpg" in tatie1 or ".webp" in tatie1:
        $ chara_ori = 1
    else:
        $ chara_ori = 0

    $ tatie2 = mon_labo_mon_ini.data.tatie2
    $ tatie3 = mon_labo_mon_ini.data.tatie3
    $ tatie4 = mon_labo_mon_ini.data.tatie4
    $ monster_pos = xy(mon_labo_mon_ini.data.monster_x, mon_labo_mon_ini.data.monster_y)
    $ tukix = mon_labo_mon_ini.ruka.tukix
    $ alice_skill = mon_labo_mon_ini.ruka.alice
    $ haikei = mon_labo_mon_ini.data.haikei

    if ".jpg" in haikei or ".webp" in haikei:
        $ bg_ori = 1
    else:
        $ bg_ori = 0

    $ max_enemylife = mon_labo_mon_ini.data.hp
    $ damage_keigen = mon_labo_mon_ini.data.keigen
    $ earth_keigen = mon_labo_mon_ini.data.earth_keigen
    $ kaihi = mon_labo_mon_ini.data.kaihi

    if alice_skill:
        $ alice_skill1 = mon_labo_mon_ini.data.alice_skill1
        $ alice_skill2 = mon_labo_mon_ini.data.alice_skill2
        $ alice_skill3 = mon_labo_mon_ini.data.alice_skill3

    $ sub1 = mon_labo_mon_ini.data.music

    if ".ogg" in str(sub1) or ".mp3" in str(sub1) or ".wav" in str(sub1):
        $ mus = 99
        $ tuika_bgm = sub1
    else:
        $ mus = sub1

    $ ori_name = mon_labo_mon_ini.ruka.name
    $ mylv = mon_labo_mon_ini.ruka.lv

    if mylv > 99:
        $ mylv = 99

    $ skill01 = mon_labo_mon_ini.ruka.skill
    $ skill02 = mon_labo_mon_ini.ruka.skill_wind

    if skill02 == 1:
        $ skill02 = 0
        $ skill03 = mon_labo_mon_ini.ruka.skill_earth
        $ skill03 = 0
        $ skill04 = mon_labo_mon_ini.ruka.skill_aqua
        $ skill04 = 0
        $ skill05 = mon_labo_mon_ini.ruka.skill_fire
        $ skill05 = 0

    call maxmp
    $ mp = max_mp

    if mon_labo_on == 1:
        $ sub1 = mon_labo_mon_ini.ruka.sp_minus

        if sub1:
            $ mp -= sub1

    $ sub1 = mon_labo_mon_ini.data.type

    if sub1 == 0:
        call syutugen
    elif sub1 == 1:
        call syutugen2

    $ mogaku_anno1 = mon_labo_mon_ini.anno.mogaku_anno1
    $ mogaku_anno2 = mon_labo_mon_ini.anno.mogaku_anno2
    $ mogaku_anno3 = mon_labo_mon_ini.anno.mogaku_anno3
    $ mogaku_anno4 = mon_labo_mon_ini.anno.mogaku_anno4
    $ mogaku_sel1 = mon_labo_mon_ini.sel.mogaku_sel1
    $ mogaku_sel2 = mon_labo_mon_ini.sel.mogaku_sel2
    $ mogaku_sel3 = mon_labo_mon_ini.sel.mogaku_sel3
    $ mogaku_sel4 = mon_labo_mon_ini.sel.mogaku_sel4
    $ mogaku_sel5 = mon_labo_mon_ini.sel.mogaku_sel5
    $ mogaku_dassyutu1 = mon_labo_mon_ini.sel.mogaku_dassyutu1
    $ mogaku_dassyutu2 = mon_labo_mon_ini.sel.mogaku_dassyutu2
    $ mogaku_earth_dassyutu1 = mon_labo_mon_ini.sel.mogaku_earth_dassyutu1
    $ mogaku_earth_dassyutu2 = mon_labo_mon_ini.sel.mogaku_earth_dassyutu2
    $ half_s1 = mon_labo_mon_ini.sel.half_s1
    $ half_s2 = mon_labo_mon_ini.sel.half_s2
    $ kiki_s1 = mon_labo_mon_ini.sel.kiki_s1
    $ kiki_s2 = mon_labo_mon_ini.sel.kiki_s2

    $ max_owaza_count1a = mon_labo_mon_ini.skilla1.skill_count

    if max_owaza_count1a > 1:
        $ tuika_owaza_count1a = max_owaza_count1a - 1

    $ max_owaza_count1b = mon_labo_mon_ini.skilla2.skill_count

    if max_owaza_count1b > 0:
        $ tuika_owaza_count1b = max_owaza_count1b - 1

    $ max_owaza_count1c = mon_labo_mon_ini.skilla3.skill_count

    if max_owaza_count1c > 0:
        $ tuika_owaza_count1c = max_owaza_count1c - 1

    $ max_owaza_count1d = mon_labo_mon_ini.skilla4.skill_count

    if max_owaza_count1d > 0:
        $ tuika_owaza_count1d = max_owaza_count1d - 1

    $ max_owaza_count1e = mon_labo_mon_ini.skilla5.skill_count

    if max_owaza_count1e > 0:
        $ tuika_owaza_count1e = max_owaza_count1e - 1

    $ max_owaza_count1f = mon_labo_mon_ini.skilla6.skill_count

    if max_owaza_count1f > 0:
        $ tuika_owaza_count1f = max_owaza_count1f - 1

    $ max_owaza_count1g = mon_labo_mon_ini.skilla7.skill_count

    if max_owaza_count1g > 0:
        $ tuika_owaza_count1g = max_owaza_count1g - 1

    $ max_owaza_count1h = mon_labo_mon_ini.skilla8.skill_count

    if max_owaza_count1h > 0:
        $ tuika_owaza_count1h = max_owaza_count1h - 1

    $ max_owaza_count2a = mon_labo_mon_ini.skillb1.skill_count

    if max_owaza_count2a > 0:
        $ tuika_owaza_count2a = max_owaza_count2a - 1

    $ max_owaza_count2b = mon_labo_mon_ini.skillb2.skill_count

    if max_owaza_count2b > 0:
        $ tuika_owaza_count2b = max_owaza_count2b - 1

    $ max_owaza_count2c = mon_labo_mon_ini.skillb3.skill_count

    if max_owaza_count2c > 0:
        $ tuika_owaza_count2c = max_owaza_count2c - 1

    $ max_owaza_count2d = mon_labo_mon_ini.skillb4.skill_count

    if max_owaza_count2d > 0:
        $ tuika_owaza_count2d = max_owaza_count2d - 1

    $ max_owaza_count3a = mon_labo_mon_ini.skillc1.skill_count

    if max_owaza_count3a > 0:
        $ tuika_owaza_count3a = max_owaza_count3a - 1

    $ max_owaza_count3b = mon_labo_mon_ini.skillc2.skill_count

    if max_owaza_count3b > 0:
        $ tuika_owaza_count3b = max_owaza_count3b - 1

    $ max_owaza_count3c = mon_labo_mon_ini.skillc3.skill_count

    if max_owaza_count3c > 0:
        $ tuika_owaza_count3c = max_owaza_count3c - 1

    $ max_owaza_count3d = mon_labo_mon_ini.skillc4.skill_count

    if max_owaza_count3d > 0:
        $ tuika_owaza_count3d = max_owaza_count3d - 1

    $ max_owaza_count4a = mon_labo_mon_ini.skilltame0.skill_count

    if max_owaza_count4a > 0:
        $ tuika_owaza_count4a = max_owaza_count4a - 1

    $ max_owaza_count4b = mon_labo_mon_ini.skillcounter0.skill_count

    if max_owaza_count4b > 0:
        $ tuika_owaza_count4b = max_owaza_count4b - 1

    $ Difficulty = 1
    $ mon_labo_mon_ini_ky = 1
    return


label mls_cmd_battle_st:

    jump common_main


label tuika_main:

    if status == 7:
        jump tuika_konran_a

    call cmd("attack")

    if result == 1 and kousoku > 0:
        if kousoku_type == 0:
            jump common_mogaku
        elif kousoku_type == 1:
            jump common_miss3
        elif kousoku_type == 2:
            $ abairitu = 80
            jump common_miss2
    elif result == 1:
        jump common_attack
    elif result == 2 and kousoku_type < 2 and earth > 0:
        if genmogaku > 0:
            jump mogaku_earth1
        elif genmogaku == 0:
            jump mogaku_earth2
    elif result == 2 and kousoku_type < 2:
        if genmogaku > 0:
            jump common_mogaku
        elif genmogaku == 0:
            jump common_mogaku2
    elif result == 2 and kousoku_type == 2 and earth > 0:
        jump mogaku_earth1
    elif result == 2 and kousoku_type == 2:
        jump common_mogaku
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump tuika_kousan
    elif result == 6:
        jump tuika_onedari
    elif result == 7:
        jump common_skill
    elif result == 8:
        jump common_alice_skill

    jump tuika_main


label tuika_konran_a:

    $ ransu = renpy.random.randint(1, 3)

    if ransu == 1:
        jump common_attack
    elif ransu == 2:
        jump tuika_konran_a1
    elif ransu == 3:
        jump tuika_konran_a2


label tuika_konran_a1:

    $ ransu = renpy.random.randint(1, 3)

    if player == 99:
        if ransu == 1:
            ori "[mon_labo_mon_ini.hero_sel.hero_attack1]!t]"
        elif ransu == 2:
            ori "[mon_labo_mon_ini.hero_sel.hero_attack2]!t]"
        elif ransu == 3:
            ori "[mon_labo_mon_ini.hero_sel.hero_attack3]!t]"
    elif ransu == 1:
        ori "ていっ！"
    elif ransu == 2:
        ori "てやっ！"
    elif ransu == 3:
        ori "でやっ！"

    "[ori_name!t]は自分自身に攻撃した！"

    call mp_kaihuku
    play sound "audio/se/slash.ogg"
    $ hanyo1 = mylv * 9 / 2
    $ hanyo2 = mylv * 11 / 2
    call damage((hanyo1, hanyo2))

    if mylife == 0:
        jump tuika_h_konran

    jump tuika_a



label tuika_konran_a2:

    $ ransu = renpy.random.randint(1, 6)

    if player == 99:
        if ransu < 3:
            ori "[mon_labo_mon_ini.hero_sel.hero_konran1]!t]"
        elif ransu < 5:
            ori "[mon_labo_mon_ini.hero_sel.hero_konran2]!t]"
        elif ransu < 7:
            ori "[mon_labo_mon_ini.hero_sel.hero_konran3]!t]"
    elif ransu == 1:
        ori "真の勇者に僕はなる！"
    elif ransu == 2:
        ori "ぷるぷる、ボクはいいスライムだよ。"
    elif ransu == 3:
        ori "えへへ、お腹いっぱい……"
    elif ransu == 4:
        ori "うわぁ、お花畑だぁ……"
    elif ransu == 5:
        ori "ここはどこなんだぁ！"
    elif ransu == 6:
        ori "僕の母さんは天使だったんだよ。"

    "[ori_name!t]は意味不明な事を口走っている……！"

    jump tuika_a


label tuika_yuwaku_a:

    jump tuika_a


label tuika_dasyutu:

    call face(param=3)

    # name $ monster_name

    $ sel1 = mogaku_dassyutu1
    $ sel2 = mogaku_dassyutu2
    $ sel3 = ""
    call selprint

    $ kousoku = 0
    call status_print
    play sound "audio/se/dassyutu.ogg"

    "[mogaku_anno4!t]"

    call face(param=1)
    jump common_main


label tuika_v:

    call face(param=3)

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.sel.vic_sel1
    $ sel2 = mon_labo_mon_ini.sel.vic_sel2
    $ sel3 = ""
    call selprint(monster_name)

    $ sub1 = mon_labo_mon_ini.data.vic_effevt

    if sub1 == 0:
        play sound "audio/se/syometu.ogg"
    elif sub1 == 2:
        play sound "audio/se/bun.ogg"

    $ renpy.hide(img_tag)
    with crash

    $ sel1 = mon_labo_mon_ini.anno.vic_anno1
    $ sel2 = mon_labo_mon_ini.anno.vic_anno2
    $ sel3 = ""
    call selprint
    $ battle = 0
    $ alice_skill = 0

    if aqua == 2:
        show expression haikei as bg with Dissolve(1.5)

    play music "audio/bgm/fanfale.ogg" noloop

    hide screen hp
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0

    "[monster_name!t] was defeated!"

    $ zukanon = 0
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_victory"
    # jump monster_labo_story_start
    $ renpy.jump(mon_labo_mon_ini_sc)


label tuika_a:

    if kousoku > 0 and kousoku_type == 2 and kousoku_hp < 0:
        jump tuika_dasyutu

    if bougyo_attack == 1 and bougyo == 0:
        call tuika_tame_hit
        jump common_main
    elif bougyo_attack == 1 and bougyo == 1:
        call tuika_tame_miss
        jump common_main

    if counter > 1:
        call tuika_counter_mid
        jump common_main
    elif counter == 1:
        call tuika_counter_end
        jump common_main

    $ tuika_skilla1 = mon_labo_mon_ini.skilla1.skill_kakuritu
    $ tuika_skilla2 = mon_labo_mon_ini.skilla2.skill_kakuritu
    $ tuika_skilla3 = mon_labo_mon_ini.skilla3.skill_kakuritu
    $ tuika_skilla4 = mon_labo_mon_ini.skilla4.skill_kakuritu
    $ tuika_skilla5 = mon_labo_mon_ini.skilla5.skill_kakuritu
    $ tuika_skilla6 = mon_labo_mon_ini.skilla6.skill_kakuritu
    $ tuika_skilla7 = mon_labo_mon_ini.skilla7.skill_kakuritu
    $ tuika_skilla8 = mon_labo_mon_ini.skilla8.skill_kakuritu
    $ tuika_skillb1 = mon_labo_mon_ini.skillb1.skill_kakuritu
    $ tuika_skillb2 = mon_labo_mon_ini.skillb2.skill_kakuritu
    $ tuika_skillb3 = mon_labo_mon_ini.skillb3.skill_kakuritu
    $ tuika_skillb4 = mon_labo_mon_ini.skillb4.skill_kakuritu
    $ tuika_skillc1 = mon_labo_mon_ini.skillc1.skill_kakuritu
    $ tuika_skillc2 = mon_labo_mon_ini.skillc2.skill_kakuritu
    $ tuika_skillc3 = mon_labo_mon_ini.skillc3.skill_kakuritu
    $ tuika_skillc4 = mon_labo_mon_ini.skillc4.skill_kakuritu
    $ tuika_skille1 = mon_labo_mon_ini.skilltame0.skill_kakuritu
    $ tuika_skillf1 = mon_labo_mon_ini.skillcounter0.skill_kakuritu

    while kousoku == 1:
        $ ransu = renpy.random.randint(1, 100)

        if tuika_skillb1 > 0 and ransu <= tuika_skillb1 and tuika_owaza_count2a >= max_owaza_count2a:
            call tuika_b1
            jump common_main

        $ ransu -= tuika_skillb1

        if tuika_skillb2 > 0 and ransu <= tuika_skillb2 and tuika_owaza_count2b >= max_owaza_count2b:
            call tuika_b2
            jump common_main

        $ ransu -= tuika_skillb2

        if tuika_skillb3 > 0 and ransu <= tuika_skillb3 and tuika_owaza_count2c >= max_owaza_count2c:
            call tuika_b3
            jump common_main

        $ ransu -= tuika_skillb3

        if tuika_skillb4 > 0 and ransu <= tuika_skillb4 and tuika_owaza_count2d >= max_owaza_count2d:
            call tuika_b4
            jump common_main

    while kousoku == 2:
        $ ransu = renpy.random.randint(1, 100)

        if tuika_skillc1 > 0 and ransu <= tuika_skillc1 and tuika_owaza_count3a >= max_owaza_count3a:
            call tuika_c1
            jump common_main

        $ ransu -= tuika_skillc1

        if tuika_skillc2 > 0 and ransu <= tuika_skillc2 and tuika_owaza_count2b >= max_owaza_count3b:
            call tuika_c2
            jump common_main

        $ ransu -= tuika_skillc2

        if tuika_skillc3 > 0 and ransu <= tuika_skillc3 and tuika_owaza_count3c >= max_owaza_count3c:
            call tuika_c3
            jump common_main

        $ ransu -= tuika_skillc3

        if tuika_skillc4 > 0 and ransu <= tuika_skillc4 and tuika_owaza_count3d >= max_owaza_count3d:
            call tuika_c4
            jump common_main

    while True:
        $ ransu = renpy.random.randint(1, 100)

        if tuika_skilla1 > 0 and ransu <= tuika_skilla1 and tuika_owaza_count1a >= max_owaza_count1a:
            call tuika_a1

            jump common_main

        $ ransu -= tuika_skilla1

        if tuika_skilla2 > 0 and ransu <= tuika_skilla2 and tuika_owaza_count1b >= max_owaza_count1b:
            call tuika_a2

            jump common_main

        $ ransu -= tuika_skilla2

        if tuika_skilla3 > 0 and ransu <= tuika_skilla3 and tuika_owaza_count1c >= max_owaza_count1c:
            call tuika_a3

            jump common_main

        $ ransu -= tuika_skilla3

        if tuika_skilla4 > 0 and ransu <= tuika_skilla4 and tuika_owaza_count1d >= max_owaza_count1d:
            call tuika_a4
            jump common_main

        $ ransu -= tuika_skilla4

        if tuika_skilla5 > 0 and ransu <= tuika_skilla5 and tuika_owaza_count1e >= max_owaza_count1e:
            call tuika_a5
            jump common_main

        $ ransu -= tuika_skilla5

        if tuika_skilla6 > 0 and ransu <= tuika_skilla6 and tuika_owaza_count1f >= max_owaza_count1f:
            call tuika_a6
            jump common_main

        $ ransu -= tuika_skilla6

        if tuika_skilla7 > 0 and ransu <= tuika_skilla7 and tuika_owaza_count1g >= max_owaza_count1g:
            call tuika_a7
            jump common_main

        $ ransu -= tuika_skilla7

        if tuika_skilla8 > 0 and ransu <= tuika_skilla8 and tuika_owaza_count1h >= max_owaza_count1h:
            call tuika_a8
            jump common_main

        $ ransu -= tuika_skilla8

        if tuika_skille1 > 0 and ransu <= tuika_skille1 and tuika_owaza_count4a >= max_owaza_count4a:
            call tuika_tame_zyunbi
            jump common_main

        $ ransu -= tuika_skille1

        if tuika_skillf1 > 0 and ransu <= tuika_skillf1 and tuika_owaza_count4b >= max_owaza_count4b and kousan == 0:
            call tuika_counter_start
            jump common_main


label tuika_ax:
    $ tuika_skilld1 = mon_labo_mon_ini.skilld1.skill_kakuritu
    $ tuika_skilld2 = mon_labo_mon_ini.skilld2.skill_kakuritu
    $ tuika_skilld3 = mon_labo_mon_ini.skilld3.skill_kakuritu
    $ tuika_skilld4 = mon_labo_mon_ini.skilld4.skill_kakuritu

label .tuika_ax2:
    $ ransu = renpy.random.randint(1, 100)

    if ransu <= tuika_skillb1 and tuika_skilld1 > 0:
        call tuika_d1
        jump tuika_ax

    $ ransu -= tuika_skillb1

    if ransu <= tuika_skillb2 and tuika_skilld2 > 0:
        call tuika_d2
        jump tuika_ax

    $ ransu -= tuika_skillb2

    if ransu <= tuika_skillb3 and tuika_skilld3 > 0:
        call tuika_d3
        jump tuika_ax

    $ ransu -= tuika_skillb3

    if ransu <= tuika_skillb4 and tuika_skilld4 > 0:
        call tuika_d4
        jump tuika_ax

    if kousoku == 3:
        jump .tuika_ax2



label tuika_a_common:

    $ tuika_skill_kouka = mon_labo_mon_ini.get_ini(skill_tag, "skill_kouka")

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel3")
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_name")

    if sub1 != "":
        call show_skillname(sub1)

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_ct1")

    if sub1:
        $ ctx = mon_labo_mon_ini.get_ini(skill_tag, "skill_ctx")
        $ cty = mon_labo_mon_ini.get_ini(skill_tag, "skill_cty")

        if ".jpg" in sub1 or ".webp" in sub1:
            $ cut_ori = 1
            $ renpy.show(sub1, at_list=[xy(ctx, cty)], tag="ct", zorder=15)
        else:
            $ cut_ori = 0
            $ renpy.show(sub1, at_list=[xy(ctx, cty)], tag="ct", zorder=15)

        $ renpy.transition(dissolve, layer="master")

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se1")

    if sub1:
        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_3")

    if sel3:
        $ rnd_ev = 1

    call selprintn

    if daten > 0:
        jump skill22x

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_aqua")
    $ ransu = renpy.random.randint(1, 100)

    if aqua > 1 and ransu < sub1:
        hide ct with dissolve
        call aqua_guard
        return

    $ wind_kakuritu = mon_labo_mon_ini.get_ini(skill_tag, "skill_wind")
    call wind_guard_kakuritu

    if wind_guard_on == 1:
        $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_windanno")
        hide ct with dissolve
        call wind_guard
        return

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")
    $ sub2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_2")

    if tuika_skill_kouka >= 2 and tuika_skill_kouka <= 5 and sub1 + sub2 == 0:
        pass
    else:
        if sub1 + sub2 > 0:
            call damage((sub1, sub2))

    if tuika_skill_kouka == 1:
        call enemylife_kaihuku

    if tuika_skill_kouka >= 3 and tuika_skill_kouka <= 5:
        call tuika_kousoku

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage2_1")

    if sub1 != 0:
        call tuika_damage2

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage3_1")

    if sub1 != 0:
        call tuika_damage3

    if tuika_skill_kouka == 6:
        call lvdrain

    if tuika_skill_kouka == 2:
        call tuika_status

    call hide_skillname
    pause .3

    if mylife == 0:
        return_to tuika_h

    if mylv_sin != mylv and mylv == 1:
        return_to tuika_hlv

    if kousan < 2:
        hide ct with dissolve

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx3")

    if sel3:
        $ rnd_ev = 1

    call selprint(monster_name)
    # name ""

    if kousoku == 3 and kousoku < 2:
        jump tuika_ax

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return


label tuika_damage2:

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se2")

    if sub1:
        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno2_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno2_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno2_3")

    if sel3:
        $ rnd_ev = 1

    call selprintn

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage2_1")
    $ sub2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage2_2")
    call damage((sub1, sub2))

    if tuika_skill_kouka == 1:
        call enemylife_kaihuku

    if tuika_skill_kouka == 6:
        call lvdrain

    return


label tuika_damage3:

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se3")

    if sub1:
        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno3_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno3_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno3_3")

    if sel3:
        $ rnd_ev = 1

    call selprintn

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage3_1")
    $ sub2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage3_2")
    call damage((sub1, sub2))

    if tuika_skill_kouka == 1:
        call enemylife_kaihuku

    if tuika_skill_kouka == 6:
        call lvdrain

    return



label tuika_status:

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_status")

    if status == sub1:
        if sub1 == 1 and status == 1:
            "[ori_name!t]は恍惚に浸っている……"
        elif sub1 == 2 and status == 2:
            "[ori_name!t]の体は麻痺している……"
        elif sub1 == 4 and status == 4:
            "[ori_name!t]は命令に逆らえない……"
        elif sub1 == 5 and status == 5:
            "[ori_name!t]は誘惑されている……"
        elif sub1 == 7 and status == 7:
            "[ori_name!t]は混乱している…………"

        call hide_skillname
        pause .3

        if mylife == 0:
            return_to tuika_h

        if kousan < 2:
            hide ct with dissolve

        return

    if sub1 == 1 and status == 0:
        $ status = 1
        $ sel1 = ori_name + _("は恍惚に浸ってしまった！")
        $ sel2 = _("ふぁぁぁぁ……")
        call status_print
    elif sub1 == 2 and status == 0:
        $ status = 2
        $ sel1 = ori_name + _("の体は麻痺してしまった！")
        $ sel2 = _("うぅぅぅ……")
        call status_print
    elif sub1 == 4 and status == 0:
        $ status = 4
        $ sel1 = ori_name + _("は命令に逆らえなくなってしまった！")
        $ sel2 = _("あぐ、うぅぅ……")
        call status_print
    elif sub1 == 5 and status == 0:
        $ status = 5
        $ sel1 = ori_name + _("は誘惑されてしまった！")
        $ sel2 = _("ふぁぁぁぁ……")
        call status_print
    elif sub1 == 7 and status == 0:
        $ status = 7
        $ sel1 = ori_name + _("は混乱してしまった！")
        $ sel2 = _(".........!?")
        call status_print

    "[sel1!t]"

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_turn1")
    $ sub2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_turn2")
    $ status_turn = renpy.random.randint(sub1, sub2)

    return


label tuika_kousoku:

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "tatie4")

    if sub1:
        $ tatie4 = sub1

    if tuika_skill_kouka == 3:
        $ kousoku = 1
    elif tuika_skill_kouka == 4:
        $ kousoku = 2
    elif tuika_skill_kouka == 5:
        $ kousoku = 3

    call status_print

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno")

    "[sel1!t]"

    $ kousoku_type = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_type")

    if kousoku_type < 2 and earth == 0:
        $ genmogaku = mon_labo_mon_ini.get_ini(skill_tag, "skill_mogaku")
    elif kousoku_type < 2 and earth > 0:
        $ genmogaku = mon_labo_mon_ini.get_ini(skill_tag, "skill_mogaku_earth")
    elif kousoku_type == 2:
        $ kousoku_hp = mon_labo_mon_ini.get_ini(skill_tag, "skill_mogaku_hp")

    return


label tuika_a1:

    $ skill_tag = "skilla1"
    $ tuika_owaza_count1a = 0
    jump tuika_a_common



label tuika_a2:

    $ skill_tag = "skilla2"
    $ tuika_owaza_count1b = 0
    jump tuika_a_common



label tuika_a3:

    $ skill_tag = "skilla3"
    $ tuika_owaza_count1c = 0
    jump tuika_a_common



label tuika_a4:

    $ skill_tag = "skilla4"
    $ tuika_owaza_count1d = 0
    jump tuika_a_common



label tuika_a5:

    $ skill_tag = "skilla5"
    $ tuika_owaza_count1e = 0
    jump tuika_a_common



label tuika_a6:

    $ skill_tag = "skilla6"
    $ tuika_owaza_count1f = 0
    jump tuika_a_common


label tuika_a7:

    $ skill_tag = "skilla7"
    $ tuika_owaza_count1g = 0
    jump tuika_a_common


label tuika_a8:

    $ skill_tag = "skilla8"
    $ tuika_owaza_count1h = 0
    jump tuika_a_common


label tuika_b1:

    $ skill_tag = "skillb1"
    $ tuika_owaza_count2a = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_b2:

    $ skill_tag = "skillb2"
    $ tuika_owaza_count2b = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_b3:

    $ skill_tag = "skillb3"
    $ tuika_owaza_count2c = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_b4:

    $ skill_tag = "skillb4"
    $ tuika_owaza_count2d = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_c1:

    $ skill_tag = "skillc1"
    $ tuika_owaza_count3a = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_c2:

    $ skill_tag = "skillc2"
    $ tuika_owaza_count3b = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_c3:

    $ skill_tag = "skillc3"
    $ tuika_owaza_count3c = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_c4:

    $ skill_tag = "skillc4"
    $ tuika_owaza_count3d = 0

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_d1:

    $ skill_tag = "skilld1"

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_d2:

    $ skill_tag = "skilld2"

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common


label tuika_d3:

    $ skill_tag = "skilld3"

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common



label tuika_d4:

    $ skill_tag = "skilld4"

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_kousoku_anno0")

    if sel1:
        "[sel1!t]"

    jump tuika_a_common



label tuika_tame_zyunbi:

    $ skill_tag = "skilltame0"
    $ bougyo_attack = 1

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel3")
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_name")

    if sub1 != "":
        call show_skillname(sub1)

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_ct1")

    if sub1:
        $ ctx = mon_labo_mon_ini.get_ini(skill_tag, "skill_ctx")
        $ cty = mon_labo_mon_ini.get_ini(skill_tag, "skill_cty")
        # $ sub3, sub2 = sub1.split(".")

        if ".jpg" in sub1 or ".webp" in sub1:
            $ cut_ori = 1
            show expression sub1 at xy(ctx, cty) as ct zorder 15
        else:
            $ cut_ori = 0
            show expression sub1 at xy(ctx, cty) as ct zorder 15

        with dissolve

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se1")

    if sub1 != "":
        # $ sub2, sub3 = sub1.split(".")

        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_3")

    if sel3 != "":
        $ rnd_ev = 1

    call selprint

    call hide_skillname
    pause .3

    if kousan < 2:
        hide ct with dissolve

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx3")

    if sel3:
        $ rnd_ev = 1

    call selprint(monster_name)
    # name ""

    return


label tuika_tame_miss:

    $ skill_tag = "skilltame1"
    $ tuika_owaza_count4a = 0
    $ bougyo_attack = 0

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel3")
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_name")

    if sub1:
        call show_skillname(sub1)

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_3")

    if sel3:
        $ rnd_ev = 1

    call selprintn

    play sound "audio/se/miss.ogg"

    "But it was dodged!"

    call hide_skillname
    pause .3

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selmiss1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selmiss2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selmiss3")

    if sel3:
        $ rnd_ev = 1

    call selprint(monster_name)
    # name ""

    return



label tuika_tame_hit:

    $ skill_tag = "skilltame1"
    $ tuika_owaza_count4a = 0
    $ bougyo_attack = 0
    jump tuika_a_common


label tuika_counter_start:

    $ skill_tag = "skillcounter0"
    $ counter = mon_labo_mon_ini.get_ini(skill_tag, "skill_counter_turn")
    $ counter += 1

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel3")
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_name")

    if sub1:
        call show_skillname(sub1)

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_ct1")

    if sub1:
        $ ctx = mon_labo_mon_ini.get_ini(skill_tag, "skill_ctx")
        $ cty = mon_labo_mon_ini.get_ini(skill_tag, "skill_cty")
        # $ sub3, sub2 = sub1.split(".")

        if ".jpg" in sub1 or ".webp" in sub1:
            $ cut_ori = 1
            show expression sub1 at xy(ctx, cty) as ct zorder 15
        else:
            $ cut_ori = 0
            show expression sub1 at xy(ctx, cty) as ct zorder 15

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_counter_tatie")

    if sub1:
        # if chara_ori == 1:
        $ renpy.show(sub1, at_list=[monster_pos])
        # elif chara_ori == 0:
            # $ renpy.show(sub1, at_list=[monster_pos])

        with dissolve

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se1")

    if sub1:

        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno1_3")

    if sel3:
        $ rnd_ev = 1

    call selprint

    call hide_skillname
    pause .3

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_selx3")

    if sel3:
        $ rnd_ev = 1

    call selprint(monster_name)
    # name ""

    return


label tuika_counter_mid:

    $ skill_tag = "skillcounter0"
    $ counter -= 1

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_mid1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_mid2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_mid3")
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_name")

    if sub1:
        call show_skillname(sub1)

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_se1")

    if sub1:
        if ".ogg" in sub1 or ".wav" in sub1:
            $ renpy.music.play("audio/mods/" + sub1, channel="sound")
        else:
            $ renpy.music.play("audio/se/" + sub1 + ".ogg", channel="sound")

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_mid1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_mid2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_mid3")

    if sel3:
        $ rnd_ev = 1

    call selprint

    call hide_skillname
    pause .3

    return



label tuika_counter_end:

    $ skill_tag = "skillcounter0"
    $ tuika_owaza_count4b = 0
    $ counter = 0

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_end1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_end2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_sel_counter_end3")
    call selprint(monster_name)
    # name ""

    call face(param=1)

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_end1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_end2")
    $ sel3 = mon_labo_mon_ini.get_ini(skill_tag, "skill_anno_counter_end3")

    if sel3:
        $ rnd_ev = 1

    call selprint

    return



label tuika_counter:

    $ skill_tag = "skillcounter1"
    $ tuika_owaza_count4b = 0
    $ counter = 0
    jump tuika_a_common



label tuika_kousan:

    call kousan_syori

    "[ori_name!t]は誘惑に屈し、無抵抗になった。"

    call face(param=2)

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.sel.kousan1
    $ sel2 = mon_labo_mon_ini.sel.kousan2
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    jump tuika_a



label tuika_onedari:
    $ on = OnedariObj()

    $ on.list01 = mon_labo_mon_ini.onedari.onedari_list1

    if on.list01:
        $ on.list01_unlock = 1

    $ on.list02 = mon_labo_mon_ini.onedari.onedari_list2

    if on.list02:
        $ on.list02_unlock = 1

    $ on.list03 = mon_labo_mon_ini.onedari.onedari_list3

    if on.list03:
        $ on.list03_unlock = 1

    $ on.list04 = mon_labo_mon_ini.onedari.onedari_list4

    if on.list04:
        $ on.list04_unlock = 1

    $ on.list05 = mon_labo_mon_ini.onedari.onedari_list5

    if on.list05:
        $ on.list05_unlock = 1

    $ on.list06 = mon_labo_mon_ini.onedari.onedari_list6

    if on.list06:
        $ on.list06_unlock = 1

    $ on.list07 = mon_labo_mon_ini.onedari.onedari_list7

    if on.list07:
        $ on.list07_unlock = 1

    $ on.list08 = mon_labo_mon_ini.onedari.onedari_list8

    if on.list08:
        $ on.list08_unlock = 1

    $ on.list09 = mon_labo_mon_ini.onedari.onedari_list9

    if on.list09:
        $ on.list09_unlock = 1

    $ on.list10 = mon_labo_mon_ini.onedari.onedari_list10

    if on.list10:
        $ on.list10_unlock = 1

    $ on.list11 = mon_labo_mon_ini.onedari.onedari_list11

    if on.list11:
        $ on.list11_unlock = 1

    $ on.list12 = mon_labo_mon_ini.onedari.onedari_list12

    if on.list12:
        $ on.list12_unlock = 1

    $ on.list13 = mon_labo_mon_ini.onedari.onedari_list13

    if on.list13:
        $ on.list13_unlock = 1

    $ on.list14 = mon_labo_mon_ini.onedari.onedari_list14

    if on.list14:
        $ on.list14_unlock = 1

    $ on.list15 = mon_labo_mon_ini.onedari.onedari_list15

    if on.list15:
        $ on.list15_unlock = 1

    $ on.list16 = mon_labo_mon_ini.onedari.onedari_list16

    if on.list16:
        $ on.list16_unlock = 1

    $ result = on.cmd()

    if result == 1:
        $ sub1 = on.list1
    elif result == 2:
        $ sub1 = on.list2
    elif result == 3:
        $ sub1 = on.list3
    elif result == 4:
        $ sub1 = on.list4
    elif result == 5:
        $ sub1 = on.list5
    elif result == 6:
        $ sub1 = on.list6
    elif result == 7:
        $ sub1 = on.list7
    elif result == 8:
        $ sub1 = on.list8
    elif result == 9:
        $ sub1 = on.list9
    elif result == 10:
        $ sub1 = on.list10
    elif result == 11:
        $ sub1 = on.list11
    elif result == 12:
        $ sub1 = on.list12
    elif result == 13:
        $ sub1 = on.list13
    elif result == 14:
        $ sub1 = on.list14
    elif result == 15:
        $ sub1 = on.list15
    elif result == 16:
        $ sub1 = on.list16
    elif result == -1:
        jump tuika_main

    $ sub2 = mon_labo_mon_ini.skillcounter1.skill_name

    if sub1 == sub2:
        jump tuika_onedarif1

    $ sub2 = mon_labo_mon_ini.skilltame1.skill_name

    if sub1 == sub2:
        jump tuika_onedarie1

    $ sub2 = mon_labo_mon_ini.skilld1.skill_name

    if sub1 == sub2:
        jump tuika_onedarid1

    $ sub2 = mon_labo_mon_ini.skilld2.skill_name

    if sub1 == sub2:
        jump tuika_onedarid2

    $ sub2 = mon_labo_mon_ini.skilld3.skill_name

    if sub1 == sub2:
        jump tuika_onedarid3

    $ sub2 = mon_labo_mon_ini.skilld4.skill_name

    if sub1 == sub2:
        jump tuika_onedarid4

    $ sub2 = mon_labo_mon_ini.skillb1.skill_name

    if sub1 == sub2:
        jump tuika_onedarib1

    $ sub2 = mon_labo_mon_ini.skillb2.skill_name

    if sub1 == sub2:
        jump tuika_onedarib2

    $ sub2 = mon_labo_mon_ini.skillb3.skill_name

    if sub1 == sub2:
        jump tuika_onedarib3

    $ sub2 = mon_labo_mon_ini.skillb4.skill_name

    if sub1 == sub2:
        jump tuika_onedarib4

    $ sub2 = mon_labo_mon_ini.skillc1.skill_name

    if sub1 == sub2:
        jump tuika_onedaric1

    $ sub2 = mon_labo_mon_ini.skillc2.skill_name

    if sub1 == sub2:
        jump tuika_onedaric2

    $ sub2 = mon_labo_mon_ini.skillc3.skill_name

    if sub1 == sub2:
        jump tuika_onedaric3

    $ sub2 = mon_labo_mon_ini.skillc4.skill_name

    if sub1 == sub2:
        jump tuika_onedaric4

    $ sub2 = mon_labo_mon_ini.skilla1.skill_name

    if sub1 == sub2:
        jump tuika_onedaria1

    $ sub2 = mon_labo_mon_ini.skilla2.skill_name

    if sub1 == sub2:
        jump tuika_onedaria2

    $ sub2 = mon_labo_mon_ini.skilla3.skill_name

    if sub1 == sub2:
        jump tuika_onedaria3

    $ sub2 = mon_labo_mon_ini.skilla4.skill_name

    if sub1 == sub2:
        jump tuika_onedaria4

    $ sub2 = mon_labo_mon_ini.skilla5.skill_name

    if sub1 == sub2:
        jump tuika_onedaria5

    $ sub2 = mon_labo_mon_ini.skilla6.skill_name

    if sub1 == sub2:
        jump tuika_onedaria6

    $ sub2 = mon_labo_mon_ini.skilla7.skill_name

    if sub1 == sub2:
        jump tuika_onedaria7

    $ sub2 = mon_labo_mon_ini.skilla8.skill_name

    if sub1 == sub2:
        jump tuika_onedaria8

    jump tuika_onedari



label tuika_onedaria1:

    call onedari_syori
    $ skill_tag = "skilla1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a1
        jump tuika_a

    while True:
        call tuika_a1


label tuika_onedaria2:

    call onedari_syori
    $ skill_tag = "skilla2"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a2
        jump tuika_a

    while True:
        call tuika_a2



label tuika_onedaria3:

    call onedari_syori
    $ skill_tag = "skilla3"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a3
        jump tuika_a

    while True:
        call tuika_a3



label tuika_onedaria4:

    call onedari_syori
    $ skill_tag = "skilla4"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a4
        jump tuika_a

    while True:
        call tuika_a4



label tuika_onedaria5:

    call onedari_syori
    $ skill_tag = "skilla5"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a5
        jump tuika_a

    while True:
        call tuika_a5



label tuika_onedaria6:

    call onedari_syori
    $ skill_tag = "skilla6"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a6
        jump tuika_a

    while True:
        call tuika_a6



label tuika_onedaria7:

    call onedari_syori
    $ skill_tag = "skilla7"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a7
        jump tuika_a

    while True:
        call tuika_a7



label tuika_onedaria8:

    call onedari_syori
    $ skill_tag = "skilla8"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_a8
        jump tuika_a

    while True:
        call tuika_a8



label tuika_onedarib1:

    if kousoku != 1:
        call onedari_syori
    elif kousoku == 1:
        call onedari_syori_k

    $ skill_tag = "skillb1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku !=1:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a8

    while True:
        call tuika_b1



label tuika_onedarib2:

    if kousoku != 1:
        call onedari_syori
    elif kousoku == 1:
        call onedari_syori_k

    $ skill_tag = "skillb2"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 1:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a8

    while True:
        call tuika_b2



label tuika_onedarib3:

    if kousoku != 1:
        call onedari_syori

    if kousoku == 1:
        call onedari_syori_k

    $ skill_tag = "skillb3"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 1:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a8

    while True:
        call tuika_b3



label tuika_onedarib4:

    if kousoku != 1:
        call onedari_syori

    if kousoku == 1:
        call onedari_syori_k

    $ skill_tag = "skillb4"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 1:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a8

    while True:
        call tuika_b4


label tuika_onedaric1:

    if kousoku != 2:
        call onedari_syori

    if kousoku == 2:
        call onedari_syori_k

    $ skill_tag = "skillc1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 2:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a8

    while True:
        call tuika_c1



label tuika_onedaric2:

    if kousoku != 2:
        call onedari_syori

    if kousoku == 2:
        call onedari_syori_k

    $ skill_tag = "skillc2"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 2:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a8

    while True:
        call tuika_c2



label tuika_onedaric3:

    if kousoku != 2:
        call onedari_syori

    if kousoku == 2:
        call onedari_syori_k

    $ skill_tag = "skillc3"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 2:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a8


    while True:
        call tuika_c3



label tuika_onedaric4:

    if kousoku != 4:
        call onedari_syori

    if kousoku == 4:
        call onedari_syori_k

    $ skill_tag = "skillc1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 2:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a8

    while True:
        call tuika_c4



label tuika_onedarid1:

    if kousoku != 3:
        call onedari_syori

    if kousoku == 3:
        call onedari_syori_k

    $ skill_tag = "skilld1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 3:
        call tuika_onedari_kousoku0

    while True:
        call tuika_d1



label tuika_onedarid2:

    if kousoku != 3:
        call onedari_syori

    if kousoku == 3:
        call onedari_syori_k

    $ skill_tag = "skilld2"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 3:
        call tuika_onedari_kousoku0

    while True:
        call tuika_d2



label tuika_onedarid3:

    if kousoku != 3:
        call onedari_syori

    if kousoku == 3:
        call onedari_syori_k

    $ skill_tag = "skilld3"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 3:
        call tuika_onedari_kousoku0

    while True:
        call tuika_d3



label tuika_onedarid4:

    if kousoku != 3:
        call onedari_syori

    if kousoku == 3:
        call onedari_syori_k

    $ skill_tag = "skilld4"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    if kousoku != 3:
        call tuika_onedari_kousoku0

    while True:
        call tuika_d4



label tuika_onedari_kousoku0:
    $ tuika_skill_kouka = mon_labo_mon_ini.skillb1.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku1

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb2.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku1

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb3.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku1

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb4.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku1


    $ tuika_skill_kouka = mon_labo_mon_ini.skillc1.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku2

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc2.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku2

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc3.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku2

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc4.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku2


    $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

    if tuika_skill_kouka == 5:
        jump tuika_onedari_kousoku3

    while True:
        "拘束技の設定にミスがあります。"



label tuika_onedari_kousoku1:

    if kousoku != 1:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 3:
            call tuika_a8


    $ tuika_skill_kouka = mon_labo_mon_ini.skillb1.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_b1

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb2.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_b2

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb3.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_b3

    $ tuika_skill_kouka = mon_labo_mon_ini.skillb4.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_b4

    return



label tuika_onedari_kousoku2:

    if kousoku != 2:
        $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a1

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a2

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a3

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a4

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a5

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a6

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a7

        $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

        if tuika_skill_kouka == 4:
            call tuika_a8


    $ tuika_skill_kouka = mon_labo_mon_ini.skillc1.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_c1

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc2.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_c2

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc3.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_c3

    $ tuika_skill_kouka = mon_labo_mon_ini.skillc4.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_c4

    return



label tuika_onedari_kousoku3:
    $ tuika_skill_kouka = mon_labo_mon_ini.skilla1.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a1

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla2.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a2

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla3.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a3

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla4.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a4

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla5.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a5

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla6.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a6

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla7.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a7

    $ tuika_skill_kouka = mon_labo_mon_ini.skilla8.skill_kouka

    if tuika_skill_kouka == 5:
        call tuika_a8

    return



label tuika_onedarie1:

    call onedari_syori
    $ skill_tag = "skilltame1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_tame_hit
        jump tuika_a

    while True:
        call tuika_tame_hit



label tuika_onedarif1:

    call onedari_syori
    $ skill_tag = "skillcounter1"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_onedari2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_damage1_1")

    if sub1 == "":
        call tuika_counter
        jump tuika_a

    while True:
        call tuika_counter



label tuika_h:

    $ ikigoe = mon_labo_mon_ini.get_ini(skill_tag, "skill_rukalose")
    call ikigoe

    if ikigoe != 4:
        call syasei1
        $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_bukkake")

        if sub1:
            if mon_labo_mon_ini.get_ini(skill_tag, "skill_bukkake_spot") == 0 and chara_ori == 1:
                $ renpy.show(sub1, at_list=[monster_pos], tag="bk", zorder=10)
            elif mon_labo_mon_ini.get_ini(skill_tag, "skill_bukkake_spot") == 0 and chara_ori == 0:
                $ renpy.show(sub1, at_list=[monster_pos], tag="bk", zorder=10)
            elif mon_labo_mon_ini.get_ini(skill_tag, "skill_bukkake_spot") == 1 and chara_ori == 1:
                $ renpy.show(sub1, at_list=[monster_pos])
            elif mon_labo_mon_ini.get_ini(skill_tag, "skill_bukkake_spot") == 1 and chara_ori == 0:
                $ renpy.show(sub1, at_list=[monster_pos])

        $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_ct2")

        if sub1:
            $ ctx = mon_labo_mon_ini.get_ini(skill_tag, "skill_ctx")
            $ cty = mon_labo_mon_ini.get_ini(skill_tag, "skill_cty")

            if cut_ori == 0:
                $ renpy.show(sub1, at_list=[xy(ctx, cty)], tag="ct", zorder=15)
            elif cut_ori == 1:
                $ renpy.show(sub1, at_list=[xy(ctx, cty)], tag="ct", zorder=15)

        call syasei2

    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_anno1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_anno2")
    $ sel3 = ""
    call selprint
    $ alice_skill = 0

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_overkill")

    if sub1 != "":
        jump tuika_overkill

    $ battle = 0

    if aqua == 2 and mon_labo_on == 0:
        show expression haikei as bg with Dissolve(1.5)

    hide screen hp
    $ renpy.stop_predict_screen("hp")
    $ renpy.stop_predict_screen("attack")
    $ renpy.stop_predict_screen("luka_skill")
    $ renpy.stop_predict_screen("onedari")
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0


    if ikigoe != 4:
        $ sel1 = _("イかされ")
    elif ikigoe == 4:
        $ sel1 = _("屈服し")

    "[ori_name!t]は、[monster_name!t]に[sel1!t]てしまった……"

    play music "audio/bgm/ero1.ogg"
    $ persistent.hsean = 1
    call face(param=2)

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ zukanon = 0
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_lose"
    # jump monster_labo_story_start
    $ renpy.jump(mon_labo_mon_ini_sc)



label tuika_hlv:

    ori "あぅぅぅぅぅ……"

    if player == 99:
        $ sel1 = mon_labo_mon_ini.hero_sel.hero_lose_lvdrain

    ori "[sel1!t]"
    # name ""
    "全身から力が失われ、立っていられない。{w}\nどうやら、ありったけの力を尻尾で搾り取られてしまったようだ。"

    $ sub1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_overkill")

    if sub1 != "":
        jump tuika_overkill

    $ battle = 0

    # if aqua=2 and mon_labo_on=1 and bg_ori=1 bg $mon_labo_mon_folder+"\"+$haikei,10,1500
    if aqua == 2 and mon_labo_on == 0:
        show expression haikei as bg with Dissolve(1.5)

    hide screen hp
    $ renpy.stop_predict_screen("hp")
    $ renpy.stop_predict_screen("attack")
    $ renpy.stop_predict_screen("luka_skill")
    $ renpy.stop_predict_screen("onedari")
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0

    "[ori_name!t]は、[monster_name!t]に屈服してしまった……"

    # if con_hbgm == 0:
    play music "audio/bgm/ero1.ogg"
    # elif con_hbgm == 1:
    #     stop music

    $ persistent.hsean = 1
    call face(param=2)

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ zukanon = 0
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_lose"
    # jump monster_labo_story_start
    $ renpy.jump(mon_labo_mon_ini_sc)


label tuika_h_konran:

    ori "あぐ、うぅぅぅ……"

    if player == 99:
        $ sel1 = mon_labo_mon_ini.hero_sel.hero_lose_konran

    ori "[sel1!t]"

    # name ""

    "自分で与えたダメージは深く、これ以上は立ってはいられない。{w}\nそのまま地面に倒れ、戦意を失ってしまった――"

    $ battle = 0

    # if aqua=2 and mon_labo_on=1 and bg_ori=1 bg $mon_labo_mon_folder+"\"+$haikei,10,1500
    if aqua == 2 and mon_labo_on == 0:
        show expression haikei as bg

    hide screen hp
    $ renpy.stop_predict_screen("hp")
    $ renpy.stop_predict_screen("attack")
    $ renpy.stop_predict_screen("luka_skill")
    $ renpy.stop_predict_screen("onedari")
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0

    "[ori_name!t]は、[name!t]に屈服してしまった……"

    play music "audio/bgm/ero1.ogg"
    $ persistent.hsean = 1
    call face(param=2)
    $ zukanon = 0
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_lose"
    $ renpy.jump(mon_labo_mon_ini_sc)



label tuika_overkill:

    if aqua == 2 and mon_labo_on == 0:
        show expression haikei as bg

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    # call elm_print
    $ bougyo = 0
    $ kousan = 3
    $ sel_hphalf = 1
    $ sel_kiki = 1
    $ mylife = 0

    if ikigoe != 4:
        $ sel1 = _("イかされ")
    elif ikigoe == 4:
        $ sel1 = _("屈服し")

    "[ori_name!t]は、[monster_name!t]に[sel1!t]てしまった……"

    # if con_hbgm == 0:
    play music "audio/bgm/ero1.ogg"

    # name $ monster_name
    $ sel1 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel1")
    $ sel2 = mon_labo_mon_ini.get_ini(skill_tag, "skill_lose_sel2")
    $ sel3 = ""
    call selprint(monster_name)
    # name ""

    $ zukanon = 0
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_" + sub1
    # jump monster_labo_story_start
    $ renpy.jump(mon_labo_mon_ini_sc)


label mls_cmd_overkill_end:

    hide screen hp
    $ renpy.stop_predict_screen("hp")
    $ renpy.stop_predict_screen("attack")
    $ renpy.stop_predict_screen("luka_skill")
    $ renpy.stop_predict_screen("onedari")
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ battle = 0
    $ persistent.hsean = 1
    $ mon_labo_mon_ini_ky = 0
    $ mon_labo_mon_ini_sc = pre_label + "_lose"
    $ renpy.jump(mon_labo_mon_ini_sc)


label syasei_clear:
    hide bk
    hide bk2
    hide bk3
    hide ct
    hide ct2
    hide ct3
    with dissolve
    return

# label addon_default:

init python:
    class Data():
        def __init__(self):
            self.name = ""
            self.pre_label = ""
            self.tatie1 = ""
            self.tatie2 = ""
            self.tatie3 = ""
            self.tatie4 = ""
            self.haikei = ""
            self.monster_x = 0
            self.monster_y = 0
            self.hp = 0
            self.keigen = 0
            self.earth_keigen = 0
            self.kaihi = ""
            self.music = ""
            self.type = ""
            self.vic_effevt = ""

    class Ruka():
        def __init__(self):
            self.name = ""
            self.lv = 0
            self.hp_minus = 0
            self.sp_minus = 0
            self.skill = ""
            self.skill_wind = ""
            self.skill_earth = ""
            self.skill_aqua = ""
            self.skill_fire = ""
            self.tukix = ""
            self.alice = 0

    class HeroSel():
        def __init__(self):
            self.hero_attack1 = ""
            self.hero_attack2 = ""
            self.hero_attack3 = ""
            self.hero_mogaku1 = ""
            self.hero_mogaku2 = ""
            self.hero_mogaku3 = ""
            self.hero_konran1 = ""
            self.hero_konran2 = ""
            self.hero_konran3 = ""
            self.hero_kiki1_1 = ""
            self.hero_kiki1_2 = ""
            self.hero_kiki1_3 = ""
            self.hero_kiki2_1 = ""
            self.hero_kiki2_2 = ""
            self.hero_kiki2_3 = ""
            self.hero_lose1_1 = ""
            self.hero_lose1_2 = ""
            self.hero_lose1_3 = ""
            self.hero_lose1_4 = ""
            self.hero_lose2_1 = ""
            self.hero_lose2_2 = ""
            self.hero_lose2_3 = ""
            self.hero_lose2_4 = ""
            self.hero_lose3_1 = ""
            self.hero_lose3_2 = ""
            self.hero_lose3_3 = ""
            self.hero_lose3_4 = ""
            self.hero_lose4_1 = ""
            self.hero_lose4_2 = ""
            self.hero_lose4_3 = ""
            self.hero_lose4_4 = ""
            self.hero_lose5_1 = ""
            self.hero_lose5_2 = ""
            self.hero_lose5_3 = ""
            self.hero_lose5_4 = ""
            self.hero_lose_lvdrain = ""
            self.hero_lose_konran = ""

    class HeroSkill1():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill2():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill3():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill4():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill5():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill6():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill7():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill8():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill9():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill10():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill11():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill12():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill13():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill14():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill15():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill16():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class HeroSkill17():
        def __init__(self):
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""

    class Anno():
        def __init__(self):
            self.mogaku_anno1 = ""
            self.mogaku_anno2 = ""
            self.mogaku_anno3 = ""
            self.mogaku_anno4 = ""
            self.vic_anno1 = ""
            self.vic_anno2 = ""

    class Sel():
        def __init__(self):
            self.mogaku_sel1 = ""
            self.mogaku_sel2 = ""
            self.mogaku_sel3 = ""
            self.mogaku_sel4 = ""
            self.mogaku_sel5 = ""
            self.mogaku_dassyutu1 = ""
            self.mogaku_dassyutu2 = ""
            self.mogaku_earth_dassyutu1 = ""
            self.mogaku_earth_dassyutu2 = ""
            self.half_s1 = ""
            self.half_s2 = ""
            self.kiki_s1 = ""
            self.kiki_s2 = ""
            self.kousan1 = ""
            self.kousan2 = ""
            self.vic_sel1 = ""
            self.vic_sel2 = ""

    class Onedari():
        def __init__(self):
            self.onedari_list1 = ""
            self.onedari_list2 = ""
            self.onedari_list3 = ""
            self.onedari_list4 = ""
            self.onedari_list5 = ""
            self.onedari_list6 = ""
            self.onedari_list7 = ""
            self.onedari_list8 = ""
            self.onedari_list9 = ""
            self.onedari_list10 = ""
            self.onedari_list11 = ""
            self.onedari_list12 = ""
            self.onedari_list13 = ""
            self.onedari_list14 = ""
            self.onedari_list15 = ""
            self.onedari_list16 = ""

    class Skilla1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla2():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla3():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla4():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla5():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla6():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla7():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilla8():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillb1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillb2():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillb3():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillb4():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillc1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillc2():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillc3():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillc4():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_anno0 = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilld1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_kousoku_anno0 = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilld2():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_kousoku_anno0 = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilld3():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_kousoku_anno0 = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilld4():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_kouka = 0
            self.skill_kousoku_anno0 = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skilltame0():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""

    class Skilltame1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_selmiss1 = ""
            self.skill_selmiss2 = ""
            self.skill_selmiss3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class Skillcounter0():
        def __init__(self):
            self.skill_name = ""
            self.skill_kakuritu = 0
            self.skill_count = 0
            self.tatie4 = ""
            self.skill_counter_turn = ""
            self.skill_counter_tatie = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_sel_counter_mid1 = ""
            self.skill_sel_counter_mid2 = ""
            self.skill_sel_counter_mid3 = ""
            self.skill_anno_counter_mid1 = ""
            self.skill_anno_counter_mid2 = ""
            self.skill_anno_counter_mid3 = ""
            self.skill_sel_counter_end1 = ""
            self.skill_sel_counter_end2 = ""
            self.skill_sel_counter_end3 = ""
            self.skill_anno_counter_end1 = ""
            self.skill_anno_counter_end2 = ""
            self.skill_anno_counter_end3 = ""

    class Skillcounter1():
        def __init__(self):
            self.skill_name = ""
            self.skill_kouka = 0
            self.skill_status = ""
            self.skill_turn1 = ""
            self.skill_turn2 = ""
            self.skill_kousoku_type = 0
            self.skill_mogaku = ""
            self.skill_mogaku_earth = ""
            self.skill_mogaku_hp = ""
            self.skill_kousoku_anno = ""
            self.skill_ct1 = ""
            self.skill_ct2 = ""
            self.skill_ctx = ""
            self.skill_cty = ""
            self.skill_wind = ""
            self.skill_windanno = ""
            self.skill_aqua = ""
            self.skill_sel1 = ""
            self.skill_sel2 = ""
            self.skill_sel3 = ""
            self.skill_anno1_1 = ""
            self.skill_anno1_2 = ""
            self.skill_anno1_3 = ""
            self.skill_se1 = ""
            self.skill_damage1_1 = 0
            self.skill_damage1_2 = 0
            self.skill_anno2_1 = ""
            self.skill_anno2_2 = ""
            self.skill_anno2_3 = ""
            self.skill_se2 = ""
            self.skill_damage2_1 = 0
            self.skill_damage2_2 = 0
            self.skill_anno3_1 = ""
            self.skill_anno3_2 = ""
            self.skill_anno3_3 = ""
            self.skill_se3 = ""
            self.skill_damage3_1 = 0
            self.skill_damage3_2 = 0
            self.skill_selx1 = ""
            self.skill_selx2 = ""
            self.skill_selx3 = ""
            self.skill_onedari1 = ""
            self.skill_onedari2 = ""
            self.skill_lose_anno1 = ""
            self.skill_lose_anno2 = ""
            self.skill_lose_sel1 = ""
            self.skill_lose_sel2 = ""
            self.skill_bukkake_spot = ""
            self.skill_bukkake = ""
            self.skill_rukalose = ""
            self.skill_overkill = ""

    class MonLaboMonIni():
        def __init__(self):
            self.data = Data()
            self.ruka = Ruka()
            self.hero_sel = HeroSel()
            self.hero_skill1 = HeroSkill1()
            self.hero_skill2 = HeroSkill2()
            self.hero_skill3 = HeroSkill3()
            self.hero_skill4 = HeroSkill4()
            self.hero_skill5 = HeroSkill5()
            self.hero_skill6 = HeroSkill6()
            self.hero_skill7 = HeroSkill7()
            self.hero_skill8 = HeroSkill8()
            self.hero_skill9 = HeroSkill9()
            self.hero_skill10 = HeroSkill10()
            self.hero_skill11 = HeroSkill11()
            self.hero_skill12 = HeroSkill12()
            self.hero_skill13 = HeroSkill13()
            self.hero_skill14 = HeroSkill14()
            self.hero_skill15 = HeroSkill15()
            self.hero_skill16 = HeroSkill16()
            self.hero_skill17 = HeroSkill17()
            self.anno = Anno()
            self.sel = Sel()
            self.onedari = Onedari()
            self.skilla1 = Skilla1()
            self.skilla2 = Skilla2()
            self.skilla3 = Skilla3()
            self.skilla4 = Skilla4()
            self.skilla5 = Skilla5()
            self.skilla6 = Skilla6()
            self.skilla7 = Skilla7()
            self.skilla8 = Skilla8()
            self.skillb1 = Skillb1()
            self.skillb2 = Skillb2()
            self.skillb3 = Skillb3()
            self.skillb4 = Skillb4()
            self.skillc1 = Skillc1()
            self.skillc2 = Skillc2()
            self.skillc3 = Skillc3()
            self.skillc4 = Skillc4()
            self.skilld1 = Skilld1()
            self.skilld2 = Skilld2()
            self.skilld3 = Skilld3()
            self.skilld4 = Skilld4()
            self.skilltame0 = Skilltame0()
            self.skilltame1 = Skilltame1()
            self.skillcounter0 = Skillcounter0()
            self.skillcounter1 = Skillcounter1()

        def get_ini(self, name, var):
            result = getattr(self, name)
            result = getattr(result, var)

            return result
