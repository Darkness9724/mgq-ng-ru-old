﻿python early:


################################################################################
# Return to Label function
#
#
    def return_to_parse(lex):
        return lex.simple_expression()

    def return_to_execute(o):
        renpy.pop_return()
        renpy.jump(o)

    renpy.register_statement("return_to", parse=return_to_parse, execute=return_to_execute)


############################################################################################
# Syokan
#
#
    def parse_syoukan(lex):
        return lex.rest()

    def execute_syoukan(o):
        store._skipping = False
        renpy.show(o, at_list=[syoukan_effect], tag='syoukan_img', zorder=30)
        renpy.pause(4.0, hard=True)
        renpy.hide('syoukan_img')
        store._skipping = True

    renpy.register_statement("syoukan", parse=parse_syoukan, execute=execute_syoukan)


############################################################################################
# Syokan2
#
#
    def parse_syoukan2(lex):
        return lex.rest()

    def execute_syoukan2(o):
        store._skipping = False
        renpy.show(o, at_list=[syoukan_effect2], tag='syoukan_img', zorder=1)
        renpy.pause(4.0, hard=True)
        renpy.hide('syoukan_img')
        store._skipping = True

    renpy.register_statement("syoukan2", parse=parse_syoukan2, execute=execute_syoukan2)

############################################################################################
# NG+
#
#
    import re

    def ngdata():
        for i in renpy.list_files():
            if i.startswith('NGDATA/images/'):
                if i.endswith('.webp'):
                    chr = i.replace('NGDATA/images/chara/','').replace('/', ' ').replace('.webp','')
                    renpy.image(chr, Image(i))
                    bg = i.replace('NGDATA/images/bg/','').replace('/', ' ').replace('.webp','')
                    renpy.image(bg, Image(i))
                    efc = i.replace('NGDATA/images/effect/','').replace('/', ' ').replace('.webp','')
                    renpy.image(efc, Image(i))
                    sys = i.replace('NGDATA/images/system/','').replace('/', ' ').replace('.webp','')
                    renpy.image(sys, Image(i))
        return

    def rnd2(n, m):
        global ren2
        while True:
            renpy.random.seed(hash(os.urandom(1000)))
            ransu = renpy.random.randint(n, m)
            
            if ransu != ren2:
                ren2 = ransu
                return ransu

    def acheck(a, *n):
        for i in range(len(a)):
            for j in n:
                if a[i] == j:
                    return True
        return False

    def vcheck(a, b):
        return True if a == b else False

    def monocro(name, r, g, b):
        r2 = float(r) / 255
        b2 = float(b) / 255
        g2 = float(g) / 255

        i = re.sub('(.*)/', '', name)
        i = i.replace('.webp','')
        renpy.image(i+"_"+str(r)+str(g)+str(b), im.MatrixColor(name,
                                    im.matrix.desaturate() * im.matrix.tint(r2, g2, b2)))
        return renpy.show(i+"_"+str(r)+str(g)+str(b))

    def gm_party_next():
        global gm_char
        if gm_char == 2 and party_member[1] != 4:
            gm_char += 1
        if gm_char == 3 and party_member[1] != 5:
            gm_char += 1
        if gm_char == 4 and party_member[1] != 3:
            gm_char += 1
        if gm_char == 5 and party_member[1] != 2:
            gm_char += 1
        if gm_char == 6 and acheck(pm,7) == False:
            gm_char += 1
        if gm_char == 7 and acheck(pm,6) == False:
            gm_char = 0

        return

    def gm_party_prev():
        global gm_char
        if gm_char == 7 and acheck(pm,6) == False:
            gm_char = 0
        if gm_char == 6 and acheck(pm,7) == False:
            gm_char -= 1
        if gm_char == 5 and party_member[1] != 2:
            gm_char -= 1
        if gm_char == 4 and party_member[1] != 3:
            gm_char -= 1
        if gm_char == 3 and party_member[1] != 5:
            gm_char -= 1
        if gm_char == 2 and party_member[1] != 4:
            gm_char -= 1

        return

    ngdata()